var captcha = {
    token: null,
    code: null
};

var formData = {
    nickname: null,
    email: null,
    gender: null,
    base64Avator: null
};

getCaptcha(captcha);

j('#captcha-img').click(
    function () {
        getCaptcha(captcha);
    }
);

j('#captcha-code').blur(
    function () {
        var code = trim(j('#captcha-code').val());
        if (code == '') {
            j('#captcha-tip .err-msg').text('验证码不能为空');
            j('#captcha-tip').show();
            captcha.code = null;
            j('#submit').css('background-color', 'gray');
        } else {
            j('#captcha-tip').hide();
            captcha.code = code;
            if (isValidated()) {
                j('#submit').attr('style', '');
            }
        }
    }
);

function isValidated() {
    return (formData.gender == 0 || formData.gender == 1) &&
        formData.email != FORM_ERR_SIGNAL && formData.base64Avator != FORM_ERR_SIGNAL &&
        captcha.token != null && captcha.code != null;
}

if (window.localStorage.jwt == undefined || window.localStorage.jwt == null || window.localStorage.jwt == '') {
    $.toast('请先登录');
} else {
    var getUserInfoAjax = j.ajax({
        url: backendAddr + '/user',
        type: 'get',
        dataType: 'json',
        headers: {
            Authorization: AUTHORIZATION_PREFIX + window.localStorage.jwt
        },
        timeout: 5000,
        success: function (data) {
            formData.nickname = data.nickname;
            formData.email = data.email;
            formData.gender = data.gender;
            j('#nickname').val(formData.nickname);
            j('#user-email').val(formData.email);
            j('#gender-' + formData.gender).attr('selected', 'selected');
        },
        error: function (resp) {
            if (resp.statusText == 'timeout') {
                getUserInfoAjax.abort();
                $.toast('系统繁忙');
                return;
            }

            if (resp.responseJSON.failed) {
                $.toast(resp.responseJSON.messages[0]);
            } else {
                $.toast('系统错误');
            }
        }
    });
}

j('#nickname').blur(function () {
    var nickname = trim(j('#nickname').val());
    j('#nickname').val(nickname);
    if (nickname != '') {
        formData.nickname = nickname;
    } else {
        formData.nickname = null;
    }
});

j('#user-email').blur(function () {
    var userEmail = trim(j('#user-email').val());
    j('#user-email').val(userEmail);
    if (userEmail == '') {
        j('#user-email-tip').hide();
        formData.email = null;
        if (isValidated()) {
            j('#submit').attr('style', '');
        }
    } else if (userEmail.search(/^(?:[a-zA-Z0-9]+[_|\_|\.]?)*[a-zA-Z0-9]+@(?:[a-zA-Z0-9]+[_|\_|\.]?)*[a-zA-Z0-9]+\.[a-zA-Z]{2,3}$/) == -1) {
        j('#user-email-tip .err-msg').text('邮箱格式错误');
        j('#user-email-tip').show();
        j('#submit').css('background-color', 'gray');
        formData.email = FORM_ERR_SIGNAL;
    } else {
        j('#user-email-tip').hide();
        formData.email = userEmail;
        if (isValidated()) {
            j('#submit').attr('style', '');
        }
    }
});

j('#gender-options').change(
    function () {
        formData.gender = parseInt(j('#gender-options option:selected').val());
    }
);

j('#avator').change(
    function () {
        var $file = $(this);
        var fileObj = $file[0];
        var dataURL;

        if (fileObj && fileObj.files && fileObj.files[0]) {
            dataURL = (window.URL || window.webkitURL).createObjectURL(fileObj.files[0]);
        } else {
            dataURL = $file.val();
        }

        if (dataURL == '') {
            j('#avator-tip').hide();
            formData.base64Avator = null;
            if (isValidated()) {
                j('#submit').attr('style', '');
            }
        } else {
            var image = new Image();
            image.src = dataURL;
            image.onload = function () {
                var base64Code = getBase64Image(image).replace(/^data:[a-zA-Z/*]+;base64,/, '');
                if (base64Code.length > 1320000) {
                    j('#avator-tip .err-msg').text('上传文件最大1MB');
                    j('#avator-tip').show();
                    formData.base64Avator = FORM_ERR_SIGNAL;
                    j('#submit').css('background-color', 'gray');
                } else {
                    formData.base64Avator = base64Code;
                    j('#avator-tip').hide();
                    if (isValidated()) {
                        j('#submit').attr('style', '');
                    }
                }
            };
            image.onerror = function () {
                j('#avator-tip .err-msg').text('上传文件类型错误，请上传.jpg，.jpeg或.png格式的文件');
                j('#avator-tip').show();
                formData.base64Avator = FORM_ERR_SIGNAL;
                j('#submit').css('background-color', 'gray');
            }
        }
    }
);

j('#submit').click(
    function () {
        if (window.localStorage.jwt == undefined || window.localStorage.jwt == null || window.localStorage.jwt == '') {
            $.toast('请先登录');
            return;
        }
        if (!isValidated()) {
            return;
        }
        if (captcha.code.length != 4) {
            $.toast('验证码错误');
            return;
        }
        var submitAjax = j.ajax({
            url: backendAddr + "/user",
            type: 'put',
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            data: JSON.stringify(formData),
            timeout: 5000,
            headers: {
                Captcha: captcha.token + '=' + captcha.code,
                Authorization: AUTHORIZATION_PREFIX + window.localStorage.jwt
            },
            success: function () {
                window.location.href = window.localStorage.from;
                window.localStorage.from = 'user_update';
            },
            error: function (resp) {
                captcha.code = null;
                getCaptcha(captcha);
                j('#captcha-code').val('');
                j('#submit').css('background-color', 'gray');

                if (resp.statusText == 'timeout') {
                    submitAjax.abort();
                    $.toast('系统繁忙');
                    return;
                }

                if (resp.responseJSON.failed) {
                    $.toast(resp.responseJSON.messages[0])
                } else {
                    $.toast('系统错误');
                }
            }
        });
    }
);

j('#cancel').click(
    function () {
        var from = window.localStorage.from;
        window.localStorage.removeItem('from');
        window.location.href = from;
    }
);
