var shopId = getShopIdFromHref();

function createRow(colName1, colName2, isHeader, isTmp, productCategoryId) {
    var row = j('<div></div>');

    if (!isTmp) {
        row.attr('class', 'row list-row');
        var col1 = j('<div></div>');
        col1.attr('class', 'col-75');
        col1.text(colName1);
        row.append(col1);
    } else {
        row.attr('class', 'row list-row tmp');
        var col1 = j('<input maxlength="12" type="text" placeholder="类别名称"/>');
        col1.attr('class', 'col-75 category-input');
        col1.blur(
            function () {
                col1.val(trim(col1.val()));
            }
        );
        row.append(col1);
        setTimeout(
            function () {
                col1.focus();
            }, 50
        )
    }

    var col2 = j('<div></div>');
    col2.attr('class', 'col-25');
    row.append(col2);
    if (isHeader) {
        col2.text(colName2);
    } else {
        var link = j('<a></a>');
        link.text(colName2);
        col2.append(link);
        link.click(
            function () {
                $.confirm('类别 ' + colName1 + ' 下的所有物品都将归为无类别，是否继续？', function () {
                    if (!row.hasClass('tmp')) {
                        if (window.localStorage.jwt == undefined || window.localStorage.jwt == null || window.localStorage.jwt == '') {
                            $.toast('请先登录');
                            return;
                        }
                        var deleteProductCategoryAjax = j.ajax({
                            url: backendAddr + "/product-category/" + productCategoryId,
                            type: 'delete',
                            contentType: 'application/json; charset=utf-8',
                            timeout: 5000,
                            headers: {
                                Authorization: AUTHORIZATION_PREFIX + window.localStorage.jwt
                            },
                            success: function () {
                                row.remove();
                                $.toast('删除成功');
                            },
                            error: function (resp) {
                                if (resp.statusText == 'timeout') {
                                    deleteProductCategoryAjax.abort();
                                    $.toast('系统繁忙');
                                    return;
                                }

                                if (resp.responseJSON.failed) {
                                    $.toast(resp.responseJSON.messages[0]);
                                } else {
                                    $.toast('系统错误');
                                }
                            }
                        })
                    } else {
                        row.remove();
                    }
                });
            }
        );
    }

    return row;
}

if (window.localStorage.jwt == undefined || window.localStorage.jwt == null || window.localStorage.jwt == '') {
    $.toast('请先登录');
} else {
    var getProductCategoryListAjax = j.ajax({
        url: backendAddr + "/product-categories",
        type: 'get',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        data: {'shop-id': shopId},
        timeout: 5000,
        async: false,
        headers: {
            Authorization: AUTHORIZATION_PREFIX + window.localStorage.jwt
        },
        success: function (data) {
            if (data.length > 0) {
                j('#content').append(createRow('类别', '操作', true, false, null));

                var categoryList = j('<div id="product-category-list"></div>');
                categoryList.attr('class', 'shop-wrap');
                j('#content').append(categoryList);

                data.sort(
                    function (a, b) {
                        return b.lastUpdateTime - a.lastUpdateTime;
                    }
                );

                for (var i = 0; i < data.length; i++) {
                    categoryList.append(createRow(data[i].name, '删除', false, false, data[i].id));
                }
            } else {
                var emptyList = j('<div id="empty-list"></div>');
                emptyList.css('color', 'gray');
                emptyList.css('width', '100%');
                emptyList.css('height', '100px');
                emptyList.css('text-align', 'center');
                j('#content').append(emptyList);

                var emptyTip = j('<p></p>');
                emptyTip.text('暂时还没有任何分类哦');
                emptyTip.css('height', '100px');
                emptyTip.css('line-height', '100px');
                emptyList.append(emptyTip);
            }
        },
        error: function (resp) {
            if (resp.statusText == 'timeout') {
                getProductCategoryListAjax.abort();
                $.toast('系统繁忙');
                return;
            }

            if (resp.responseJSON.failed) {
                $.toast(resp.responseJSON.messages[0]);
            } else {
                $.toast('系统错误');
            }
        }
    })
}

j('#add-product-categories').click(
    function () {
        var categoryList;
        if (j('#content').children().eq(0).attr('id') == 'empty-list') {
            j('#content').empty();
            j('#content').append(createRow('类别', '操作', true, false, null));

            var categoryList = j('<div id="product-category-list"></div>');
            categoryList.attr('class', 'shop-wrap');
            j('#content').append(categoryList);
        } else {
            categoryList = j('#product-category-list');
        }

        categoryList.prepend(createRow(null, '删除', false, true, null));
    }
);

j('#submit').click(
    function () {
        var categoryNames = [];
        var tmpRows = j('#product-category-list .tmp');
        for (var i = 0; i < tmpRows.length; i++) {
            var name = trim(tmpRows[i].getElementsByTagName('input')[0].value);
            if (name != undefined && name != null && name != "") {
                categoryNames.push(name);
            }
        }

        if (categoryNames.length == 0) {
            $.toast('没有新增的分类');
            return;
        }

        var addProductCategoriesAjax = j.ajax({
            url: backendAddr + "/product-categories?shop-id=" + shopId,
            type: 'post',
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            data: JSON.stringify(categoryNames),
            timeout: 5000,
            async: false,
            headers: {
                Authorization: AUTHORIZATION_PREFIX + window.localStorage.jwt
            },
            success: function () {
                $.toast('提交成功');
            },
            error: function (resp) {
                if (resp.statusText == 'timeout') {
                    addProductCategoriesAjax.abort();
                    $.toast('系统繁忙');
                    return;
                }

                if (resp.responseJSON.failed) {
                    $.toast(resp.responseJSON.messages[0]);
                } else {
                    $.toast('系统错误');
                }
            }
        });
        var getProductCategoryListAjax = j.ajax({
            url: backendAddr + "/product-categories",
            type: 'get',
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            data: {'shop-id': shopId},
            timeout: 5000,
            headers: {
                Authorization: AUTHORIZATION_PREFIX + window.localStorage.jwt
            },
            success: function (data) {
                j('#content').empty();
                if (data.length > 0) {
                    j('#content').append(createRow('类别', '操作', true, false, null));

                    var categoryList = j('<div id="product-category-list"></div>');
                    categoryList.attr('class', 'shop-wrap');
                    j('#content').append(categoryList);

                    data.sort(
                        function (a, b) {
                            return b.lastUpdateTime - a.lastUpdateTime;
                        }
                    );

                    for (var i = 0; i < data.length; i++) {
                        categoryList.append(createRow(data[i].name, '删除', false, false, data[i].id));
                    }
                } else {
                    var emptyList = j('<div id="empty-list"></div>');
                    emptyList.css('color', 'gray');
                    emptyList.css('width', '100%');
                    emptyList.css('height', '100px');
                    emptyList.css('text-align', 'center');
                    j('#content').append(emptyList);

                    var emptyTip = j('<p></p>');
                    emptyTip.text('暂时还没有任何分类哦');
                    emptyTip.css('height', '100px');
                    emptyTip.css('line-height', '100px');
                    emptyList.append(emptyTip);
                }
            },
            error: function (resp) {
                if (resp.statusText == 'timeout') {
                    getProductCategoryListAjax.abort();
                    $.toast('系统繁忙');
                    return;
                }

                if (resp.responseJSON.failed) {
                    $.toast(resp.responseJSON.messages[0]);
                } else {
                    $.toast('系统错误');
                }
            }
        })
    }
);
