var categoryId = getCategoryIdFromHref();
var hasPrevPage = false;
var hasNextPage = false;
var currentPage = 1;
var totalPage = 1;
var pageSize = 2;

j('#me').click(
    function () {
        $.openPanel('#right-panel');
    }
);

if (!(window.localStorage.jwt == undefined || window.localStorage.jwt == null || window.localStorage.jwt == '')) {
    j('#log-in-out').text('退出登录');
    j('#log-in-out').click(
        function () {
            window.localStorage.removeItem('jwt');
        }
    );
    j('#log-in-out').attr('href', './index.html');
    j('#register').hide();
    j('#modify-pwd').show();
    j('#edit-user-info').show();
    j('#my-shops').show();
    j('#my-message').show();
    var getUserInfoAjax = j.ajax({
        url: backendAddr + "/user",
        type: 'get',
        contentType: 'application/json; charset=utf-8',
        timeout: 5000,
        headers: {
            Authorization: AUTHORIZATION_PREFIX + window.localStorage.jwt
        },
        success: function (data) {
            j('#hi').text('hi, ' + data.nickname);
        },
        error: function (resp) {
            if (resp.statusText == 'timeout') {
                getUserInfoAjax.abort();
                $.toast('系统繁忙');
                return;
            }

            if (resp.responseJSON.failed) {
                $.toast(resp.responseJSON.messages[0]);
            } else {
                $.toast('系统错误');
            }
        }
    });
} else {
    j('#log-in-out').text('登录');
    j('#log-in-out').unbind();
    j('#log-in-out').click(
        function () {
            window.localStorage.from = window.location.href;
        }
    );
    j('#log-in-out').attr('href', '../htmls/sign_in.html');
    j('#register').show();
    j('#modify-pwd').hide();
    j('#edit-user-info').hide();
    j('#my-shops').hide();
    j('#my-message').hide();
    j('#hi').text('hi');
}

var getSubCategoriesAjax = j.ajax({
    url: backendAddr + '/category/sub-categories?parent-id=' + categoryId,
    type: 'get',
    contentType: 'application/json; charset=utf-8',
    timeout: 5000,
    success: function (data) {
        for (var i = 0; i < data.length; i++) {
            var button = j('<a id="' + data[i].id + '" class="button">' + data[i].name + '</a>');
            button.click(switchCategory);
            j('#shoplist-search-div').append(button);
        }
    },
    error: function (resp) {
        if (resp.statusText == 'timeout') {
            getSubCategoriesAjax.abort();
            $.toast('系统繁忙');
            return;
        }

        if (resp.responseJSON.failed) {
            $.toast(resp.responseJSON.messages[0]);
        } else {
            $.toast('系统错误');
        }
    }
});
var getAreasAjax = j.ajax({
    url: backendAddr + '/areas',
    type: 'get',
    contentType: 'application/json; charset=utf-8',
    dataType: 'json',
    timeout: 5000,
    success: function (data) {
        for (var i = 0; i < data.length; i++) {
            var option = j('<option></option>');
            option.text(data[i].name);
            option.attr('value', data[i].id);
            j('#area-search').append(option);
        }
    },
    error: function (resp) {
        if (resp.statusText == 'timeout') {
            getAreasAjax.abort();
            $.toast('系统繁忙');
            return;
        }

        if (resp.responseJSON.failed) {
            $.toast(resp.responseJSON.messages[0]);
        } else {
            $.toast('系统错误');
        }
    }
});
var getShopsAjax = j.ajax({
    url: backendAddr + '/shops-conditional?page=' + currentPage + '&size=' + pageSize + '&category-id=' + categoryId,
    type: 'get',
    contentType: 'application/json; charset=utf-8',
    timeout: 5000,
    async: false,
    success: function (data) {
        if (data.list.length == 0) {
            j('#shop-list').append(createEmptyTip('暂时还没有任何店铺哦'));
            return;
        }

        for (var i = 0; i < data.list.length; i++) {
            createShopCard(data.list[i]);
        }

        setPageBar(data);
        hasPrevPage = data.hasPreviousPage;
        hasNextPage = data.hasNextPage;
        currentPage = data.pageNum;
        totalPage = data.pages;
    },
    error: function (resp) {
        if (resp.statusText == 'timeout') {
            getShopsAjax.abort();
            $.toast('系统繁忙');
            return;
        }

        if (resp.responseJSON.failed) {
            $.toast(resp.responseJSON.messages[0]);
        } else {
            $.toast('系统错误');
        }
    }
});

j('#prev-page').click(
    function () {
        if (!hasPrevPage) {
            return;
        }

        currentPage--;
        var url = backendAddr + '/shops-conditional?page=' + currentPage + '&size=' + pageSize;
        if (j('#area-search option:selected').val() != 'all') {
            url += ('&area-id=' + j('#area-search option:selected').val());
        }
        var subCategories = j('#shoplist-search-div a');
        for (var i = 0; i < subCategories.length; i++) {
            if (j(subCategories[i]).attr('style') == 'background-color: cornflowerblue; color: white;') {
                if (j(subCategories[i]).attr('id') != 'all') {
                    url += '&category-id=' + j(subCategories[i]).attr('id');
                } else {
                    url += ('&category-id=' + categoryId);
                }
                break;
            }
        }

        var getShopListAjax = j.ajax({
            url: url,
            type: 'get',
            contentType: 'application/json; charset=utf-8',
            timeout: 5000,
            success: function (data) {
                j('#shop-list').children().remove();
                for (var i = 0; i < data.list.length; i++) {
                    createShopCard(data.list[i]);
                }

                setPageBar(data);
                hasPrevPage = data.hasPreviousPage;
                hasNextPage = data.hasNextPage;
                currentPage = data.pageNum;
                totalPage = data.pages;
            },
            error: function (resp) {
                if (resp.statusText == 'timeout') {
                    getShopListAjax.abort();
                    $.toast('系统繁忙');
                    return;
                }

                if (resp.responseJSON.failed) {
                    $.toast(resp.responseJSON.messages[0]);
                } else {
                    $.toast('系统错误');
                }
            }
        })
    }
);

j('#first-page').click(
    function () {
        if (!hasPrevPage) {
            return;
        }

        currentPage = 1;
        var url = backendAddr + '/shops-conditional?page=' + currentPage + '&size=' + pageSize;
        if (j('#area-search option:selected').val() != 'all') {
            url += ('&area-id=' + j('#area-search option:selected').val());
        }
        var subCategories = j('#shoplist-search-div a');
        for (var i = 0; i < subCategories.length; i++) {
            if (j(subCategories[i]).attr('style') == 'background-color: cornflowerblue; color: white;') {
                if (j(subCategories[i]).attr('id') != 'all') {
                    url += '&category-id=' + j(subCategories[i]).attr('id');
                } else {
                    url += ('&category-id=' + categoryId);
                }
                break;
            }
        }

        var getShopListAjax = j.ajax({
            url: url,
            type: 'get',
            contentType: 'application/json; charset=utf-8',
            timeout: 5000,
            success: function (data) {
                j('#shop-list').children().remove();
                for (var i = 0; i < data.list.length; i++) {
                    createShopCard(data.list[i]);
                }

                setPageBar(data);
                hasPrevPage = data.hasPreviousPage;
                hasNextPage = data.hasNextPage;
                currentPage = data.pageNum;
                totalPage = data.pages;
            },
            error: function (resp) {
                if (resp.statusText == 'timeout') {
                    getShopListAjax.abort();
                    $.toast('系统繁忙');
                    return;
                }

                if (resp.responseJSON.failed) {
                    $.toast(resp.responseJSON.messages[0]);
                } else {
                    $.toast('系统错误');
                }
            }
        })
    }
);

j('#next-page').click(
    function () {
        if (!hasNextPage) {
            return;
        }

        currentPage++;
        var url = backendAddr + '/shops-conditional?page=' + currentPage + '&size=' + pageSize;
        if (j('#area-search option:selected').val() != 'all') {
            url += ('&area-id=' + j('#area-search option:selected').val());
        }
        var subCategories = j('#shoplist-search-div a');
        for (var i = 0; i < subCategories.length; i++) {
            if (j(subCategories[i]).attr('style') == 'background-color: cornflowerblue; color: white;') {
                if (j(subCategories[i]).attr('id') != 'all') {
                    url += '&category-id=' + j(subCategories[i]).attr('id');
                } else {
                    url += ('&category-id=' + categoryId);
                }
                break;
            }
        }

        var getShopListAjax = j.ajax({
            url: url,
            type: 'get',
            contentType: 'application/json; charset=utf-8',
            timeout: 5000,
            success: function (data) {
                j('#shop-list').children().remove();
                for (var i = 0; i < data.list.length; i++) {
                    createShopCard(data.list[i]);
                }

                setPageBar(data);
                hasPrevPage = data.hasPreviousPage;
                hasNextPage = data.hasNextPage;
                currentPage = data.pageNum;
                totalPage = data.pages;
            },
            error: function (resp) {
                if (resp.statusText == 'timeout') {
                    getShopListAjax.abort();
                    $.toast('系统繁忙');
                    return;
                }

                if (resp.responseJSON.failed) {
                    $.toast(resp.responseJSON.messages[0]);
                } else {
                    $.toast('系统错误');
                }
            }
        })
    }
);

j('#last-page').click(
    function () {
        if (!hasNextPage) {
            return;
        }

        currentPage = totalPage;
        var url = backendAddr + '/shops-conditional?page=' + currentPage + '&size=' + pageSize;
        if (j('#area-search option:selected').val() != 'all') {
            url += ('&area-id=' + j('#area-search option:selected').val());
        }
        var subCategories = j('#shoplist-search-div a');
        for (var i = 0; i < subCategories.length; i++) {
            if (j(subCategories[i]).attr('style') == 'background-color: cornflowerblue; color: white;') {
                if (j(subCategories[i]).attr('id') != 'all') {
                    url += '&category-id=' + j(subCategories[i]).attr('id');
                } else {
                    url += ('&category-id=' + categoryId);
                }
                break;
            }
        }

        var getShopListAjax = j.ajax({
            url: url,
            type: 'get',
            contentType: 'application/json; charset=utf-8',
            timeout: 5000,
            success: function (data) {
                j('#shop-list').children().remove();
                for (var i = 0; i < data.list.length; i++) {
                    createShopCard(data.list[i]);
                }

                setPageBar(data);
                hasPrevPage = data.hasPreviousPage;
                hasNextPage = data.hasNextPage;
                currentPage = data.pageNum;
                totalPage = data.pages;
            },
            error: function (resp) {
                if (resp.statusText == 'timeout') {
                    getShopListAjax.abort();
                    $.toast('系统繁忙');
                    return;
                }

                if (resp.responseJSON.failed) {
                    $.toast(resp.responseJSON.messages[0]);
                } else {
                    $.toast('系统错误');
                }
            }
        })
    }
);

j('#register').click(
    function () {
        window.localStorage.from = window.location.href;
    }
);

j('#modify-pwd').click(
    function () {
        window.localStorage.from = window.location.href;
    }
);

j('#edit-user-info').click(function () {
    window.localStorage.from = window.location.href;
});

function switchCategory() {
    var buttons = j('#shoplist-search-div>a');
    for (var i = 0; i < buttons.length; i++) {
        if (buttons[i] != j(this)[0]) {
            j(buttons[i]).removeAttr('style');
        }
    }
    j(this).attr('style', 'background-color: cornflowerblue; color: white;');

    hasPrevPage = false;
    hasNextPage = false;
    currentPage = 1;
    totalPage = 1;
    pageSize = 2;

    var url = backendAddr + '/shops-conditional?page=' + currentPage + '&size=' + pageSize;
    if (j('#area-search option:selected').val() != 'all') {
        url += ('&area-id=' + j('#area-search option:selected').val());
    }
    if (j(this).attr('id') != 'all') {
        url += '&category-id=' + j(this).attr('id');
    } else {
        url += ('&category-id=' + categoryId);
    }

    var getShopListAjax = j.ajax({
        url: url,
        type: 'get',
        contentType: 'application/json; charset=utf-8',
        timeout: 5000,
        success: function (data) {
            j('#shop-list').children().remove();
            if (data.list.length == 0) {
                hidePageBar();
                j('#shop-list').append(createEmptyTip('暂时还没有任何店铺哦'));
                return;
            }

            for (var i = 0; i < data.list.length; i++) {
                createShopCard(data.list[i]);
            }

            setPageBar(data);
            hasPrevPage = data.hasPreviousPage;
            hasNextPage = data.hasNextPage;
            currentPage = data.pageNum;
            totalPage = data.pages;
        },
        error: function (resp) {
            if (resp.statusText == 'timeout') {
                getShopListAjax.abort();
                $.toast('系统繁忙');
                return;
            }

            if (resp.responseJSON.failed) {
                $.toast(resp.responseJSON.messages[0]);
            } else {
                $.toast('系统错误');
            }
        }
    })
}

j('#shoplist-search-div>a').click(switchCategory);

j('#area-search').change(
    function () {
        hasPrevPage = false;
        hasNextPage = false;
        currentPage = 1;
        totalPage = 1;
        pageSize = 2;

        var url = backendAddr + '/shops-conditional?page=' + currentPage + '&size=' + pageSize;
        if (j('#area-search option:selected').val() != 'all') {
            url += ('&area-id=' + j('#area-search option:selected').val());
        }
        var subCategories = j('#shoplist-search-div a');
        for (var i = 0; i < subCategories.length; i++) {
            if (j(subCategories[i]).attr('style') == 'background-color: cornflowerblue; color: white;') {
                if (j(subCategories[i]).attr('id') != 'all') {
                    url += '&category-id=' + j(subCategories[i]).attr('id');
                } else {
                    url += ('&category-id=' + categoryId);
                }
                break;
            }
        }

        var getShopListAjax = j.ajax({
            url: url,
            type: 'get',
            contentType: 'application/json; charset=utf-8',
            timeout: 5000,
            success: function (data) {
                j('#shop-list').children().remove();
                if (data.list.length == 0) {
                    hidePageBar();
                    j('#shop-list').append(createEmptyTip('暂时还没有任何店铺哦'));
                    return;
                }
                for (var i = 0; i < data.list.length; i++) {
                    createShopCard(data.list[i]);
                }

                setPageBar(data);
                hasPrevPage = data.hasPreviousPage;
                hasNextPage = data.hasNextPage;
                currentPage = data.pageNum;
                totalPage = data.pages;
            },
            error: function (resp) {
                if (resp.statusText == 'timeout') {
                    getShopListAjax.abort();
                    $.toast('系统繁忙');
                    return;
                }

                if (resp.responseJSON.failed) {
                    $.toast(resp.responseJSON.messages[0]);
                } else {
                    $.toast('系统错误');
                }
            }
        })
    }
);

j('#my-message a').click(function () {
    window.localStorage.from = window.location.href;
    window.location.href = '../htmls/my_messages.html';
});

if (window.localStorage.from == 'pwd_update') {
    $.toast('修改成功');
    window.localStorage.from = null;
}

if (window.localStorage.from == 'register') {
    $.toast('注册成功');
    window.localStorage.from = null;
}

if (window.localStorage.from == 'user_update') {
    $.toast('修改成功');
    window.localStorage.from = null;
}
