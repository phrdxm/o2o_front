var captcha = {
    token: null,
    code: null
};

getCaptcha(captcha);

if (window.localStorage.jwt == undefined || window.localStorage.jwt == null || window.localStorage.jwt == '') {
    $.toast('请先登录');
} else {
    var getAreasAjax = j.ajax({
        url: backendAddr + '/areas',
        type: 'get',
        dataType: 'json',
        headers: {
            Authorization: AUTHORIZATION_PREFIX + window.localStorage.jwt
        },
        timeout: 5000,
        success: function (data) {
            for (var i = 0; i < data.length; i++) {
                var option = j('<option></option>');
                option.text(data[i].name);
                option.attr('value', data[i].id);
                if (i == 0) {
                    option.attr('selected', 'selected');
                    formData.areaId = data[0].id;
                }
                j('#shop-area-options').append(option);
            }
        },
        error: function (resp) {
            if (resp.statusText == 'timeout') {
                getAreasAjax.abort();
                $.toast('系统繁忙');
                return;
            }

            if (resp.responseJSON.failed) {
                $.toast(resp.responseJSON.messages[0]);
            } else {
                $.toast('系统错误');
            }
        }
    });
    var getCategoriesAjax = j.ajax({
        url: backendAddr + '/category/sub-categories',
        type: 'get',
        dataType: 'json',
        headers: {
            Authorization: AUTHORIZATION_PREFIX + window.localStorage.jwt
        },
        timeout: 5000,
        success: function (data) {
            for (var i = 0; i < data.length; i++) {
                var option = j('<option></option>');
                option.text(data[i].name);
                option.attr('value', data[i].id);
                if (i == 0) {
                    option.attr('selected', 'selected');
                    formData.categoryId = data[0].id;
                }
                j('#shop-category-options').append(option);
            }
        },
        error: function (resp) {
            if (resp.statusText == 'timeout') {
                getCategoriesAjax.abort();
                $.toast('系统繁忙');
                return;
            }

            if (resp.responseJSON.failed) {
                $.toast(resp.responseJSON.messages[0]);
            } else {
                $.toast('系统错误');
            }
        }
    });
}

var formData = {
    name: null,
    description: null,
    address: null,
    phone: null,
    areaId: null,
    categoryId: null,
    base64Image: null
}

function isValidated() {
    return formData.name != null && formData.address != null &&
        formData.areaId != null && formData.categoryId != null &&
        formData.phone != FORM_ERR_SIGNAL && formData.base64Image != FORM_ERR_SIGNAL &&
        captcha.code != null && captcha.token != null;
}

j('#shop-name').blur(
    function () {
        var shopName = trim(j('#shop-name').val());
        j('#shop-name').val(shopName);
        if (shopName == '') {
            j('#shop-name-tip .err-msg').text('店铺名称不能为空');
            j('#shop-name-tip').show();
            j('#submit').css('background-color', 'gray');
            formData.name = null;
        } else {
            j('#shop-name-tip').hide();
            formData.name = shopName;
            if (isValidated()) {
                j('#submit').attr('style', '');
            }
        }
    }
)

j('#shop-desc').blur(
    function () {
        var shopDesc = trim(j('#shop-desc').val());
        j('#shop-desc').val(shopDesc);
        if (shopDesc != '') {
            formData.description = shopDesc;
        } else {
            formData.description = null;
        }
    }
);

j('#shop-addr').blur(
    function () {
        var shopAddr = trim(j('#shop-addr').val());
        j('#shop-addr').val(shopAddr);
        if (shopAddr == '') {
            j('#shop-addr-tip .err-msg').text('详细地址不能为空');
            j('#shop-addr-tip').show();
            j('#submit').css('background-color', 'gray');
            formData.address = null;
        } else {
            j('#shop-addr-tip').hide();
            formData.address = shopAddr;
            if (isValidated()) {
                j('#submit').attr('style', '');
            }
        }
    }
)

j('#shop-phone').blur(
    function () {
        var shopPhone = trim(j('#shop-phone').val());
        j('#shop-phone').val(shopPhone);
        if (shopPhone == '') {
            j('#shop-phone-tip').hide();
            formData.phone = null;
            if (isValidated()) {
                j('#submit').attr('style', '');
            }
        } else if (shopPhone.search(/^(?:13[0-9]|14[579]|15[0-3,5-9]|16[6]|17[0135678]|18[0-9]|19[89])\d{8}$/) == -1) {
            j('#shop-phone-tip .err-msg').text('手机号格式错误');
            j('#shop-phone-tip').show();
            j('#submit').css('background-color', 'gray');
            formData.phone = FORM_ERR_SIGNAL;
        } else {
            j('#shop-phone-tip').hide();
            formData.phone = shopPhone;
            if (isValidated()) {
                j('#submit').attr('style', '');
            }
        }
    }
);

j('#captcha-img').click(
    function () {
        getCaptcha(captcha);
    }
);

j('#captcha-code').blur(
    function () {
        var code = trim(j('#captcha-code').val());
        if (code == '') {
            j('#captcha-tip .err-msg').text('验证码不能为空');
            j('#captcha-tip').show();
            captcha.code = null;
            j('#submit').css('background-color', 'gray');
        } else {
            j('#captcha-tip').hide();
            captcha.code = code;
            if (isValidated()) {
                j('#submit').attr('style', '');
            }
        }
    }
);

j('#shop-img').change(
    function () {
        var $file = $(this);
        var fileObj = $file[0];
        var dataURL;

        if (fileObj && fileObj.files && fileObj.files[0]) {
            dataURL = (window.URL || window.webkitURL).createObjectURL(fileObj.files[0]);
        } else {
            dataURL = $file.val();
        }

        if (dataURL == '') {
            j('#shop-img-tip').hide();
            formData.base64Image = null;
            if (isValidated()) {
                j('#submit').attr('style', '');
            }
        } else {
            var image = new Image();
            image.src = dataURL;
            image.onload = function () {
                var base64Code = getBase64Image(image).replace(/^data:[a-zA-Z/*]+;base64,/, '');
                if (base64Code.length > 1320000) {
                    j('#shop-img-tip .err-msg').text('上传文件最大1MB');
                    j('#shop-img-tip').show();
                    formData.base64Image = FORM_ERR_SIGNAL;
                    j('#submit').css('background-color', 'gray');
                } else {
                    formData.base64Image = base64Code;
                    j('#shop-img-tip').hide();
                    if (isValidated()) {
                        j('#submit').attr('style', '');
                    }
                }
            };
            image.onerror = function () {
                j('#shop-img-tip .err-msg').text('上传文件类型错误，请上传.jpg，.jpeg或.png格式的文件');
                j('#shop-img-tip').show();
                formData.base64Image = FORM_ERR_SIGNAL;
                j('#submit').css('background-color', 'gray');
            }
        }
    }
);

j('#shop-area-options').change(
    function () {
        formData.areaId = j('#shop-area-options option:selected').val();
    }
);

j('#shop-category-options').change(
    function () {
        formData.categoryId = j('#shop-category-options option:selected').val();
    }
);

j('#submit').click(
    function () {
        if (!isValidated()) {
            return;
        }
        if (captcha.code.length != 4) {
            $.toast('验证码错误');
            return;
        }
        if (window.localStorage.jwt == undefined || window.localStorage.jwt == null || window.localStorage.jwt == '') {
            $.toast('请先登录');
        }

        var submitAjax = j.ajax({
            url: backendAddr + "/shop",
            type: 'post',
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            data: JSON.stringify(formData),
            timeout: 5000,
            headers: {
                Captcha: captcha.token + '=' + captcha.code,
                Authorization: AUTHORIZATION_PREFIX + window.localStorage.jwt
            },
            success: function () {
                window.localStorage.from = 'shop_register';
                window.location.href = '../htmls/shop_list.html';
            },
            error: function (resp) {
                captcha.code = null;
                getCaptcha(captcha);
                j('#captcha-code').val('');
                j('#submit').css('background-color', 'gray');

                if (resp.statusText == 'timeout') {
                    submitAjax.abort();
                    $.toast('系统繁忙');
                    return;
                }

                if (resp.responseJSON.failed) {
                    $.toast(resp.responseJSON.messages[0])
                } else {
                    $.toast('系统错误');
                }
            }
        })
    }
);
