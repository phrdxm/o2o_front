if (!(window.localStorage.jwt == undefined || window.localStorage.jwt == null || window.localStorage.jwt == '')) {
    j('#log-in-out').text('退出登录');
    j('#log-in-out').click(
        function () {
            window.localStorage.removeItem('jwt');
        }
    );
    j('#log-in-out').attr('href', './index.html');
    j('#register').hide();
    j('#modify-pwd').show();
    j('#edit-user-info').show();
    j('#my-shops').show();
    j('#my-message').show();
    var getUserInfoAjax = j.ajax({
        url: backendAddr + "/user",
        type: 'get',
        contentType: 'application/json; charset=utf-8',
        timeout: 5000,
        headers: {
            Authorization: AUTHORIZATION_PREFIX + window.localStorage.jwt
        },
        success: function (data) {
            j('#hi').text('hi, ' + data.nickname);
        },
        error: function (resp) {
            if (resp.statusText == 'timeout') {
                getUserInfoAjax.abort();
                $.toast('系统繁忙');
                return;
            }

            if (resp.responseJSON.failed) {
                $.toast(resp.responseJSON.messages[0]);
            } else {
                $.toast('系统错误');
            }
        }
    });

} else {
    j('#log-in-out').text('登录');
    j('#log-in-out').unbind();
    j('#log-in-out').click(
        function () {
            window.localStorage.from = window.location.href;
        }
    );
    j('#log-in-out').attr('href', '../htmls/sign_in.html');
    j('#register').show();
    j('#modify-pwd').hide();
    j('#edit-user-info').hide();
    j('#my-shops').hide();
    j('#my-message').hide();
    j('#hi').text('hi');
}

j('#me').click(
    function () {
        $.openPanel('#right-panel');
    }
);

j('#register').click(
    function () {
        window.localStorage.from = window.location.href;
    }
);

j('#modify-pwd').click(
    function () {
        window.localStorage.from = window.location.href;
    }
);

j('#edit-user-info').click(function () {
    window.localStorage.from = window.location.href;
});

j('.pull-left').click(function () {
    if (window.localStorage.from == undefined) {
        window.location.href = '../index.html';
        return;
    }
    var from = window.localStorage.from;
    window.localStorage.removeItem('from');
    window.location.href = from;
});

if (window.localStorage.jwt == undefined || window.localStorage.jwt == null || window.localStorage.jwt == '') {
    $.toast('请先登录');
} else {
    var getUsersInfoTalkingToAjax = j.ajax({
        url: backendAddr + "/message/users",
        type: 'get',
        contentType: 'application/json; charset=utf-8',
        timeout: 5000,
        headers: {
            Authorization: AUTHORIZATION_PREFIX + window.localStorage.jwt
        },
        success: function (data) {
            if (data.length > 0) {
                j('#content').append(j('<div class="list-block media-list inset"><ul></ul></div>'));
                for (var i = 0; i < data.length; i++) {
                    var img = '../images/default_avator.png';
                    if (data[i].avatorFileName != null) {
                        img = backendAddr + '/image/user-profile/' + data[i].avatorFileName;
                    }
                    var item = j('<li>' +
                        '<a href="../htmls/chat.html?owner-id=' + data[i].id + '" class="item-link item-content" external>' +
                        '<div class="item-media"><img src="' + img + '" width="44" height="44"></div>' +
                        '<div class="item-inner" style="display: flex; align-items: center">' +
                        '<span>' + data[i].nickname + '</span></div></a></li>');
                    j(j('#content').find('ul')[0]).append(item);
                }
            } else {
                j('#content').append(createEmptyTip('暂时还没有任何会话哦'));
            }
        },
        error: function (resp) {
            if (resp.statusText == 'timeout') {
                getUsersInfoTalkingToAjax.abort();
                $.toast('系统繁忙');
                return;
            }

            if (resp.responseJSON.failed) {
                $.toast(resp.responseJSON.messages[0]);
            } else {
                $.toast('系统错误');
            }
        }
    });
}

if (window.localStorage.from == 'pwd_update') {
    $.toast('修改成功');
    window.localStorage.from = null;
}

if (window.localStorage.from == 'register') {
    $.toast('注册成功');
    window.localStorage.from = null;
}

if (window.localStorage.from == 'user_update') {
    $.toast('修改成功');
    window.localStorage.from = null;
}
