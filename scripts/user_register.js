var captcha = {
    token: null,
    code: null
};

getCaptcha(captcha);

var formData = {
    username: null,
    password: null,
    nickname: null,
    email: null,
    gender: 0,
    base64Avator: null
};

function isValidated() {
    return formData.username != null && formData.password != null && (formData.gender == 0 || formData.gender == 1) &&
        formData.email != FORM_ERR_SIGNAL && formData.base64Avator != FORM_ERR_SIGNAL &&
        formData.password == j('#pwd-confirm').val() &&
        captcha.token != null && captcha.code != null;
}

j('#captcha-img').click(
    function () {
        getCaptcha(captcha);
    }
);

j('#captcha-code').blur(
    function () {
        var code = trim(j('#captcha-code').val());
        if (code == '') {
            j('#captcha-tip .err-msg').text('验证码不能为空');
            j('#captcha-tip').show();
            captcha.code = null;
            j('#submit').css('background-color', 'gray');
        } else {
            j('#captcha-tip').hide();
            captcha.code = code;
            if (isValidated()) {
                j('#submit').attr('style', '');
            }
        }
    }
);

j('#username').blur(
    function () {
        var username = trim(j('#username').val());
        j('#username').val(username);
        if (username == '') {
            j('#username-tip .err-msg').text('用户名不能为空');
            j('#username-tip').show();
            formData.username = null;
            j('#submit').css('background-color', 'gray');
        } else if (username.search(/^\w{6,16}$/) == -1) {
            j('#username-tip .err-msg').text('用户名长度为6到16个字符，只能由数字字母下划线组成');
            j('#username-tip').show();
            formData.username = null;
            j('#submit').css('background-color', 'gray');
        } else {
            j('#username-tip').hide();
            formData.username = username;
            if (isValidated()) {
                j('#submit').attr('style', '');
            }
        }
    }
);

j('#pwd').blur(
    function () {
        var pwd = j('#pwd').val();
        var pwdConfirm = j('#pwd-confirm').val();
        if (pwd == '') {
            j('#pwd-tip .err-msg').text('密码不能为空');
            j('#pwd-tip').show();
            formData.password = null;
            j('#submit').css('background-color', 'gray');
            if (pwdConfirm != '') {
                j('#pwd-confirm-tip .err-msg').text('请先填写密码');
                j('#pwd-confirm-tip').show();
            } else {
                j('#pwd-confirm-tip').hide();
            }
            return;
        }

        if (pwd.search(/^(?:\w|[~`!@#$%^&*?,:;()\-.+={}\[\]]){6,}$/) == -1) {
            j('#pwd-tip .err-msg').text('密码至少6位，只能由数字字母下划线以及特殊字符~`!@#$%^&*?,:;()\\-.+={}[]组成');
            j('#pwd-tip').show();
            formData.password = null;
            j('#submit').css('background-color', 'gray');
        } else {
            j('#pwd-tip').hide();
            formData.password = pwd;
        }

        if (pwdConfirm != '') {
            if (pwd != pwdConfirm) {
                j('#pwd-confirm-tip .err-msg').text('密码不一致');
                j('#pwd-confirm-tip').show();
                j('#submit').css('background-color', 'gray');
            } else {
                j('#pwd-confirm-tip').hide();
                if (isValidated()) {
                    j('#submit').attr('style', '');
                }
            }
        }
    }
);

j('#pwd-confirm').blur(
    function () {
        var pwd = j('#pwd').val();
        var pwdConfirm = j('#pwd-confirm').val();
        if (pwd == '') {
            if (pwdConfirm != '') {
                j('#pwd-confirm-tip .err-msg').text('请先填写密码');
                j('#pwd-confirm-tip').show();
            } else {
                j('#pwd-confirm-tip').hide();
            }
            formData.password = null;
            j('#submit').css('background-color', 'gray');
            return;
        }

        if (pwdConfirm == '') {
            j('#pwd-confirm-tip .err-msg').text('确认密码不能为空');
            j('#pwd-confirm-tip').show();
            j('#submit').css('background-color', 'gray');
        } else if (pwd != pwdConfirm) {
            j('#pwd-confirm-tip .err-msg').text('密码不一致');
            j('#pwd-confirm-tip').show();
            j('#submit').css('background-color', 'gray');
        } else {
            j('#pwd-confirm-tip').hide();
            if (isValidated()) {
                j('#submit').attr('style', '');
            }
        }
    }
);

j('#nickname').blur(
    function () {
        var nickname = trim(j('#nickname').val());
        j('#nickname').val(nickname);
        if (nickname != '') {
            formData.nickname = nickname;
        } else {
            formData.nickname = null;
        }
    }
);

j('#user-email').blur(
    function () {
        var userEmail = trim(j('#user-email').val());
        j('#user-email').val(userEmail);
        if (userEmail == '') {
            j('#user-email-tip').hide();
            formData.email = null;
            if (isValidated()) {
                j('#submit').attr('style', '');
            }
        } else if (userEmail.search(/^(?:[a-zA-Z0-9]+[_|\_|\.]?)*[a-zA-Z0-9]+@(?:[a-zA-Z0-9]+[_|\_|\.]?)*[a-zA-Z0-9]+\.[a-zA-Z]{2,3}$/) == -1) {
            j('#user-email-tip .err-msg').text('邮箱格式错误');
            j('#user-email-tip').show();
            j('#submit').css('background-color', 'gray');
            formData.email = FORM_ERR_SIGNAL;
        } else {
            j('#user-email-tip').hide();
            formData.email = userEmail;
            if (isValidated()) {
                j('#submit').attr('style', '');
            }
        }
    }
);

j('#gender-options').change(
    function () {
        formData.gender = parseInt(j('#gender-options option:selected').val());
    }
);

j('#avator').change(
    function () {
        var $file = $(this);
        var fileObj = $file[0];
        var dataURL;

        if (fileObj && fileObj.files && fileObj.files[0]) {
            dataURL = (window.URL || window.webkitURL).createObjectURL(fileObj.files[0]);
        } else {
            dataURL = $file.val();
        }

        if (dataURL == '') {
            j('#avator-tip').hide();
            formData.base64Avator = null;
            if (isValidated()) {
                j('#submit').attr('style', '');
            }
        } else {
            var image = new Image();
            image.src = dataURL;
            image.onload = function () {
                var base64Code = getBase64Image(image).replace(/^data:[a-zA-Z/*]+;base64,/, '');
                if (base64Code.length > 1320000) {
                    j('#avator-tip .err-msg').text('上传文件最大1MB');
                    j('#avator-tip').show();
                    formData.base64Avator = FORM_ERR_SIGNAL;
                    j('#submit').css('background-color', 'gray');
                } else {
                    formData.base64Avator = base64Code;
                    j('#avator-tip').hide();
                    if (isValidated()) {
                        j('#submit').attr('style', '');
                    }
                }
            };
            image.onerror = function () {
                j('#avator-tip .err-msg').text('上传文件类型错误，请上传.jpg，.jpeg或.png格式的文件');
                j('#avator-tip').show();
                formData.base64Avator = FORM_ERR_SIGNAL;
                j('#submit').css('background-color', 'gray');
            }
        }
    }
);

j('#submit').click(
    function () {
        if (!isValidated()) {
            return;
        }
        if (captcha.code.length != 4) {
            $.toast('验证码错误');
            return;
        }
        var submitAjax = j.ajax({
            url: backendAddr + "/user/register",
            type: 'post',
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            data: JSON.stringify(formData),
            timeout: 5000,
            headers: {
                Captcha: captcha.token + '=' + captcha.code
            },
            success: function (data) {
                window.location.href = window.localStorage.from;
                window.localStorage.from = 'register';
                window.localStorage.jwt = data.jwt;
            },
            error: function (resp) {
                captcha.code = null;
                getCaptcha(captcha);
                j('#captcha-code').val('');
                j('#submit').css('background-color', 'gray');

                if (resp.statusText == 'timeout') {
                    submitAjax.abort();
                    $.toast('系统繁忙');
                    return;
                }

                if (resp.responseJSON.failed) {
                    $.toast(resp.responseJSON.messages[0])
                } else {
                    $.toast('系统错误');
                }
            }
        });
    }
);

j('#cancel').click(
    function () {
        var from = window.localStorage.from;
        window.localStorage.removeItem('from');
        window.location.href = from;
    }
);
