var captcha = {
    token: null,
    code: null
};

var formData = {
    originalPassword: null,
    newPassword: null
};

getCaptcha(captcha);

j('#captcha-img').click(
    function () {
        getCaptcha(captcha);
    }
);

function isValidated() {
    return formData.originalPassword != null && formData.newPassword != null &&
        captcha.token != null && captcha.code != null &&
        formData.newPassword == j('#new-pwd-confirm').val();
}

j('#captcha-code').blur(
    function () {
        var code = trim(j('#captcha-code').val());
        if (code == '') {
            j('#captcha-tip .err-msg').text('验证码不能为空');
            j('#captcha-tip').show();
            captcha.code = null;
            j('#submit').css('background-color', 'gray');
        } else {
            j('#captcha-tip').hide();
            captcha.code = code;
            if (isValidated()) {
                j('#submit').attr('style', '');
            }
        }
    }
);

j('#cancel').click(
    function () {
        var from = window.localStorage.from;
        window.localStorage.removeItem('from');
        window.location.href = from;
    }
);

j('#original-pwd').blur(
    function () {
        var pwd = j('#original-pwd').val();
        if (pwd == '') {
            j('#original-pwd-tip .err-msg').text('密码不能为空');
            j('#original-pwd-tip').show();
            formData.originalPassword = null;
            j('#submit').css('background-color', 'gray');
        } else if (pwd.search(/^(?:\w|[~`!@#$%^&*?,:;()\-.+={}\[\]]){6,}$/) == -1) {
            j('#original-pwd-tip .err-msg').text('密码至少6位，只能由数字字母下划线以及特殊字符~`!@#$%^&*?,:;()\\-.+={}[]组成');
            j('#original-pwd-tip').show();
            formData.originalPassword = null;
            j('#submit').css('background-color', 'gray');
        } else {
            j('#original-pwd-tip').hide();
            formData.originalPassword = pwd;
            if (isValidated()) {
                j('#submit').attr('style', '');
            }
        }
    }
);

function confirmPwd() {
    if (j('#new-pwd').val() != '' && j('#new-pwd').val() != j('#new-pwd-confirm').val()) {
        j('#new-pwd-confirm-tip .err-msg').text('确认密码不一致');
        j('#new-pwd-confirm-tip').show();
        j('#submit').css('background-color', 'gray');
    } else {
        j('#new-pwd-confirm-tip').hide();
        if (isValidated()) {
            j('#submit').attr('style', '');
        }
    }
}

j('#new-pwd').blur(
    function () {
        var pwd = j('#new-pwd').val();
        if (pwd == '') {
            j('#new-pwd-tip .err-msg').text('密码不能为空');
            j('#new-pwd-tip').show();
            formData.newPassword = null;
            j('#submit').css('background-color', 'gray');
        } else if (pwd.search(/^(?:\w|[~`!@#$%^&*?,:;()\-.+={}\[\]]){6,}$/) == -1) {
            j('#new-pwd-tip .err-msg').text('密码至少6位，只能由数字字母下划线以及特殊字符~`!@#$%^&*?,:;()\\-.+={}[]组成');
            j('#new-pwd-tip').show();
            formData.newPassword = null;
            j('#submit').css('background-color', 'gray');
        } else {
            j('#new-pwd-tip').hide();
            formData.newPassword = pwd;
            if (isValidated()) {
                j('#submit').attr('style', '');
            }
        }

        if (j('#new-pwd-confirm').val() != '') {
            confirmPwd();
        }
    }
);

j('#new-pwd-confirm').blur(confirmPwd);

j('#submit').click(
    function () {
        if (!isValidated()) {
            return;
        }
        if (captcha.code.length != 4) {
            $.toast('验证码错误');
            return;
        }
        if (formData.newPassword == formData.originalPassword) {
            $.toast('新密码和原密码一致');
            return;
        }
        var submitAjax = j.ajax({
            url: backendAddr + "/user/password",
            type: 'put',
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            data: JSON.stringify(formData),
            timeout: 5000,
            headers: {
                Captcha: captcha.token + '=' + captcha.code,
                Authorization: AUTHORIZATION_PREFIX + window.localStorage.jwt
            },
            success: function () {
                var from = window.localStorage.from;
                window.localStorage.from = 'pwd_update';
                window.location.href = from;
            },
            error: function (resp) {
                if (resp.status == 201) {
                    var from = window.localStorage.from;
                    window.localStorage.from = 'pwd_update';
                    window.location.href = from;
                    return;
                }
                captcha.code = null;
                getCaptcha(captcha);
                j('#captcha-code').val('');
                j('#submit').css('background-color', 'gray');

                if (resp.statusText == 'timeout') {
                    submitAjax.abort();
                    $.toast('系统繁忙');
                    return;
                }

                if (resp.responseJSON.failed) {
                    $.toast(resp.responseJSON.messages[0])
                } else {
                    $.toast('系统错误');
                }
            }
        })
    }
);
