var backendAddr = "http://47.106.158.254:8080";
var FORM_ERR_SIGNAL = 'err';
var AUTHORIZATION_PREFIX = 'Bearer ';

function trim(x) {
    return x.replace(/^\s+|\s+$/gm, '');
}

function timeStampToDate(timeStamp) {
    return new Date(timeStamp).toLocaleString().split(' ')[0];
}

function timeStampToTime(timeStamp) {
    return new Date(timeStamp).toLocaleString();
}

function getShopIdFromHref() {
    var shopIdIndex = window.location.href.indexOf('shop-id=') + 8;
    return window.location.href.substr(shopIdIndex, 36);
}

function getProductIdFromHref() {
    var productIdIndex = window.location.href.indexOf('product-id=') + 11;
    return window.location.href.substr(productIdIndex, 36);
}

function getCategoryIdFromHref() {
    var categoryIdIndex = window.location.href.indexOf('category-id=') + 12;
    return window.location.href.substr(categoryIdIndex, 36);
}

function getOwnerIdFromHref() {
    var ownerIdIndex = window.location.href.indexOf('owner-id=') + 9;
    return window.location.href.substr(ownerIdIndex, 36);
}

function getCaptcha(captchaObj) {
    j.ajax({
        url: backendAddr + '/captcha',
        type: 'post',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            j('#captcha-img').attr('src', 'data:image/png;base64,' + data.image);
            captchaObj.token = data.token;
        },
        error: function (resp) {
            if (resp.responseJSON.failed) {
                $.toast(resp.responseJSON.messages[0]);
            } else {
                $.toast('系统错误');
            }
        }
    });
}

function getBase64Image(img) {
    var canvas = document.createElement("canvas");
    canvas.width = img.width;
    canvas.height = img.height;
    canvas.getContext("2d").drawImage(img, 0, 0, img.width, img.height);
    return canvas.toDataURL();
}

function setPageBar(pageInfo) {
    if (j('#page-bar')[0] == undefined) {
        j('#content').append(createPageBar());
    }

    j('#page-bar').show();
    j('#page-num').text(pageInfo.pageNum + '/' + pageInfo.pages);

    if (!pageInfo.hasNextPage) {
        j('#next-page').css('color', 'gray');
        j('#last-page').css('color', 'gray');
    } else {
        j('#next-page').css('color', 'black');
        j('#last-page').css('color', 'black');
    }

    if (!pageInfo.hasPreviousPage) {
        j('#prev-page').css('color', 'gray');
        j('#first-page').css('color', 'gray');
    } else {
        j('#prev-page').css('color', 'black');
        j('#first-page').css('color', 'black');
    }
}

function createPageBar() {
    var pageBar = j('<div id="page-bar"><ul class="pagination">' +
        '<li><a id="first-page">«</a></li>' +
        '<li><a id="prev-page">‹</a></li>' +
        '<li id="page-num"></li>' +
        '<li><a id="next-page">›</a></li>' +
        '<li><a id="last-page">»</a></li>' +
        '</ul></div>');
    pageBar.css('text-align', 'center');

    return pageBar;
}

function hidePageBar() {
    j('#page-bar').hide();
}

function appendShopId(elementId) {
    var link = j('#' + elementId).attr('href');
    link += ('?shop-id=' + getShopIdFromHref());
    j('#' + elementId).attr('href', link);
}

function createShopCard(item) {
    var img = '';
    if (item.imageFileName != null) {
        img = '<img src="' + backendAddr + '/image/shop/' + item.imageFileName + '" width="44" height="44"/>';
    }
    var card = j('<div class="card">' +
        '<div class="card-header">' + item.name + '</div>' +
        '<div class="card-content">' +
        '<div class="list-block media-list">' +
        '<ul>' +
        '<li class="item-content">' +
        '<div class="item-media">' +
        img +
        '</div>' +
        '<div class="item-inner">' +
        '<div class="item-subtitle">' + (item.description == null ? '没有描述' : item.description) + '</div>' +
        '</div></li></ul></div></div>' +
        '<div class="card-footer">' +
        '<span>' + timeStampToDate(item.lastUpdateTime) + '更新</span>' +
        '<span>点击查看</span>' +
        '</div></div>');
    j('#shop-list').append(card);
    j(card.find('span')[1]).click(
        function () {
            window.location.href = '../htmls/shop_detail.html?shop-id=' + item.id;
        }
    )
}

function createProductCard(item) {
    var img = '';
    if (item.imageFileName != null) {
        img = '<img src="' + backendAddr + '/image/product/' + item.imageFileName + '" width="44" height="44"/>';
    }
    var card = j('<div class="card">' +
        '<div class="card-header">' + item.name + '</div>' +
        '<div class="card-content">' +
        '<div class="list-block media-list">' +
        '<ul>' +
        '<li class="item-content">' +
        '<div class="item-media">' +
        img +
        '</div>' +
        '<div class="item-inner">' +
        '<div class="item-subtitle">' + (item.description == null ? '没有描述' : item.description) + '</div>' +
        '</div></li></ul></div></div>' +
        '<div class="card-footer">' +
        '<span>' + timeStampToDate(item.lastUpdateTime) + '更新</span>' +
        '<span>点击查看</span>' +
        '</div></div>');
    j('#product-list').append(card);
    j(card.find('span')[1]).click(
        function () {
            window.location.href = '../htmls/product_detail.html?product-id=' + item.id;
        }
    )
}

function createEmptyTip(msg) {
    var emptyList = j('<div></div>');
    emptyList.css('color', 'gray');
    emptyList.css('width', '100%');
    emptyList.css('height', '100px');
    emptyList.css('text-align', 'center');

    var emptyTip = j('<p></p>');
    emptyTip.text(msg);
    emptyTip.css('height', '100px');
    emptyTip.css('line-height', '100px');
    emptyList.append(emptyTip);

    return emptyList;
}
