var hasPrevPage = false;
var hasNextPage = false;
var currentPage = 1;
var totalPage = 1;
var pageSize = 2;

function getShopStatusName(code) {
    if (code == -1) {
        return '下线';
    } else if (code == 0) {
        return '审核中';
    } else if (code == 1) {
        return '上线';
    } else {
        return '';
    }
}

function createRow(colName1, colName2, colName3, isHeader, shopId) {
    var row = j('<div></div>');
    row.attr('class', 'row list-row');

    var col1 = j('<div></div>');
    col1.attr('class', 'col-40');
    col1.text(colName1);
    row.append(col1);

    var col2 = j('<div></div>');
    col2.attr('class', 'col-40');
    col2.text(colName2);
    row.append(col2);

    var col3 = j('<div></div>');
    col3.attr('class', 'col-20');
    row.append(col3);
    if (isHeader) {
        col3.text(colName3);
    } else {
        var link = j('<a></a>');
        link.attr('href', '../htmls/shop_manage.html?shop-id=' + shopId);
        link.text(colName3);
        col3.append(link);
    }

    return row;
}

if (window.localStorage.jwt == undefined || window.localStorage.jwt == null || window.localStorage.jwt == '') {
    $.toast('请先登录');
} else {
    var getUserInfoAjax = j.ajax({
        url: backendAddr + "/user",
        type: 'get',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        timeout: 5000,
        headers: {
            Authorization: AUTHORIZATION_PREFIX + window.localStorage.jwt
        },
        success: function (data) {
            j('#username').text(data.nickname);
        },
        error: function (resp) {
            if (resp.statusText == 'timeout') {
                getUserInfoAjax.abort();
                $.toast('系统繁忙');
                return;
            }

            if (resp.responseJSON.failed) {
                $.toast(resp.responseJSON.messages[0]);
            } else {
                $.toast('系统错误');
            }
        }
    });
    var getShopListAjax = j.ajax({
        url: backendAddr + "/shops",
        type: 'get',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        data: {page: currentPage, size: pageSize},
        timeout: 5000,
        async: false,
        headers: {
            Authorization: AUTHORIZATION_PREFIX + window.localStorage.jwt
        },
        success: function (data) {
            if (data.list.length > 0) {
                j('#content').append(createRow('店铺名称', '状态', '操作', true, null));

                var shopList = j('<div id="shop-list"></div>');
                shopList.attr('class', 'shop-wrap');
                j('#content').append(shopList);

                for (var i = 0; i < data.list.length; i++) {
                    shopList.append(createRow(data.list[i].name, getShopStatusName(data.list[i].status), '进入', false, data.list[i].id));
                }

                j('#content').append(createPageBar());
                setPageBar(data);
                hasPrevPage = data.hasPreviousPage;
                hasNextPage = data.hasNextPage;
                currentPage = data.pageNum;
                totalPage = data.pages;
            }
        },
        error: function (resp) {
            if (resp.statusText == 'timeout') {
                getShopListAjax.abort();
                $.toast('系统繁忙');
                return;
            }

            if (resp.responseJSON.messages[0] == '权限不足') {
                var emptyList = j('<div></div>');
                emptyList.css('color', 'gray');
                emptyList.css('width', '100%');
                emptyList.css('height', '100px');
                emptyList.css('text-align', 'center');
                j('#content').append(emptyList);

                var emptyTip = j('<p></p>');
                emptyTip.text('暂时还没有任何店铺哦');
                emptyTip.css('height', '100px');
                emptyTip.css('line-height', '100px');
                emptyList.append(emptyTip);

                return;
            }

            if (resp.responseJSON.failed) {
                $.toast(resp.responseJSON.messages[0]);
            } else {
                $.toast('系统错误');
            }
        }
    })
}

j('#prev-page').click(
    function () {
        if (!hasPrevPage) {
            return;
        }
        if (window.localStorage.jwt == undefined || window.localStorage.jwt == null || window.localStorage.jwt == '') {
            $.toast('请先登录');
            return;
        }

        currentPage--;
        var getShopListAjax = j.ajax({
            url: backendAddr + "/shops",
            type: 'get',
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            data: {page: currentPage, size: pageSize},
            timeout: 5000,
            headers: {
                Authorization: AUTHORIZATION_PREFIX + window.localStorage.jwt
            },
            success: function (data) {
                j('#shop-list').children().remove();
                for (var i = 0; i < data.list.length; i++) {
                    j('#shop-list').append(createRow(data.list[i].name, getShopStatusName(data.list[i].status), '进入', false, data.list[i].id));
                }

                setPageBar(data);
                hasPrevPage = data.hasPreviousPage;
                hasNextPage = data.hasNextPage;
                currentPage = data.pageNum;
                totalPage = data.pages;
            },
            error: function (resp) {
                if (resp.statusText == 'timeout') {
                    getShopListAjax.abort();
                    $.toast('系统繁忙');
                    return;
                }

                if (resp.responseJSON.failed) {
                    $.toast(resp.responseJSON.messages[0]);
                } else {
                    $.toast('系统错误');
                }
            }
        })
    }
);

j('#first-page').click(
    function () {
        if (!hasPrevPage) {
            return;
        }
        if (window.localStorage.jwt == undefined || window.localStorage.jwt == null || window.localStorage.jwt == '') {
            $.toast('请先登录');
            return;
        }

        currentPage = 1;
        var getShopListAjax = j.ajax({
            url: backendAddr + "/shops",
            type: 'get',
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            data: {page: currentPage, size: pageSize},
            timeout: 5000,
            headers: {
                Authorization: AUTHORIZATION_PREFIX + window.localStorage.jwt
            },
            success: function (data) {
                j('#shop-list').children().remove();
                for (var i = 0; i < data.list.length; i++) {
                    j('#shop-list').append(createRow(data.list[i].name, getShopStatusName(data.list[i].status), '进入', false, data.list[i].id));
                }

                setPageBar(data);
                hasPrevPage = data.hasPreviousPage;
                hasNextPage = data.hasNextPage;
                currentPage = data.pageNum;
                totalPage = data.pages;
            },
            error: function (resp) {
                if (resp.statusText == 'timeout') {
                    getShopListAjax.abort();
                    $.toast('系统繁忙');
                    return;
                }

                if (resp.responseJSON.failed) {
                    $.toast(resp.responseJSON.messages[0]);
                } else {
                    $.toast('系统错误');
                }
            }
        })
    }
);

j('#next-page').click(
    function () {
        if (!hasNextPage) {
            return;
        }
        if (window.localStorage.jwt == undefined || window.localStorage.jwt == null || window.localStorage.jwt == '') {
            $.toast('请先登录');
            return;
        }

        currentPage++;
        var getShopListAjax = j.ajax({
            url: backendAddr + "/shops",
            type: 'get',
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            data: {page: currentPage, size: pageSize},
            timeout: 5000,
            headers: {
                Authorization: AUTHORIZATION_PREFIX + window.localStorage.jwt
            },
            success: function (data) {
                j('#shop-list').children().remove();
                for (var i = 0; i < data.list.length; i++) {
                    j('#shop-list').append(createRow(data.list[i].name, getShopStatusName(data.list[i].status), '进入', false, data.list[i].id));
                }

                setPageBar(data);
                hasPrevPage = data.hasPreviousPage;
                hasNextPage = data.hasNextPage;
                currentPage = data.pageNum;
                totalPage = data.pages;
            },
            error: function (resp) {
                if (resp.statusText == 'timeout') {
                    getShopListAjax.abort();
                    $.toast('系统繁忙');
                    return;
                }

                if (resp.responseJSON.failed) {
                    $.toast(resp.responseJSON.messages[0]);
                } else {
                    $.toast('系统错误');
                }
            }
        })
    }
);

j('#last-page').click(
    function () {
        if (!hasNextPage) {
            return;
        }
        if (window.localStorage.jwt == undefined || window.localStorage.jwt == null || window.localStorage.jwt == '') {
            $.toast('请先登录');
            return;
        }

        currentPage = totalPage;
        var getShopListAjax = j.ajax({
            url: backendAddr + "/shops",
            type: 'get',
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            data: {page: currentPage, size: pageSize},
            timeout: 5000,
            headers: {
                Authorization: AUTHORIZATION_PREFIX + window.localStorage.jwt
            },
            success: function (data) {
                j('#shop-list').children().remove();
                for (var i = 0; i < data.list.length; i++) {
                    j('#shop-list').append(createRow(data.list[i].name, getShopStatusName(data.list[i].status), '进入', false, data.list[i].id));
                }

                setPageBar(data);
                hasPrevPage = data.hasPreviousPage;
                hasNextPage = data.hasNextPage;
                currentPage = data.pageNum;
                totalPage = data.pages;
            },
            error: function (resp) {
                if (resp.statusText == 'timeout') {
                    getShopListAjax.abort();
                    $.toast('系统繁忙');
                    return;
                }

                if (resp.responseJSON.failed) {
                    $.toast(resp.responseJSON.messages[0]);
                } else {
                    $.toast('系统错误');
                }
            }
        })
    }
);

if (window.localStorage.from == 'shop_register') {
    $.toast('注册完成');
    window.localStorage.from = null;
} else if (window.localStorage.from == 'delete_shop') {
    $.toast('删除成功');
    window.localStorage.from = null;
}
