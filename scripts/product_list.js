appendShopId('back');
appendShopId('new-product');

var hasPrevPage = false;
var hasNextPage = false;
var currentPage = 1;
var totalPage = 1;
var pageSize = 2;

var shopId = getShopIdFromHref();

function enableLinkOnClick() {
    var available;
    var enableLink = j(this);
    if (enableLink.text() == '下架') {
        available = false;
    } else {
        available = true;
    }

    var ajax = j.ajax({
        url: backendAddr + "/product",
        type: 'put',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        data: JSON.stringify({id: enableLink.data('product-id'), available: available}),
        timeout: 5000,
        headers: {
            Authorization: AUTHORIZATION_PREFIX + window.localStorage.jwt
        },
        success: function () {
            if (enableLink.text() == '下架') {
                enableLink.text('上架');
                $.toast('下架成功');
            } else {
                enableLink.text('下架');
                $.toast('上架成功');
            }
        },
        error: function (resp) {
            if (resp.statusText == 'timeout') {
                ajax.abort();
                $.toast('系统繁忙');
                return;
            }

            if (resp.responseJSON.failed) {
                $.toast(resp.responseJSON.messages[0]);
            } else {
                $.toast('系统错误');
            }
        }
    });
}

function deleteLinkOnClick() {
    var deleteLink = j(this);
    $.confirm('删除的物品无法恢复，你确定吗？', function () {
        var deleteProductAjax = j.ajax({
            url: backendAddr + "/product/" + deleteLink.data('product-id'),
            type: 'delete',
            contentType: 'application/json; charset=utf-8',
            timeout: 5000,
            headers: {
                Authorization: AUTHORIZATION_PREFIX + window.localStorage.jwt
            },
            success: function () {
                window.localStorage.from = 'delete_product';
                window.location.reload();
            },
            error: function (resp) {
                if (resp.statusText == 'timeout') {
                    deleteProductAjax.abort();
                    $.toast('系统繁忙');
                    return;
                }

                if (resp.responseJSON.failed) {
                    $.toast(resp.responseJSON.messages[0]);
                } else {
                    $.toast('系统错误');
                }
            }
        });
    });
}

function createRow(colName1, colName2, colName3, isHeader, productId) {
    var row = j('<div></div>');
    row.css('display', 'table');
    row.css('width', '100%');
    row.attr('class', 'list-row');

    var col1 = j('<div></div>');
    col1.css('display', 'table-cell');
    col1.css('width', '40%');
    col1.css('vertical-align', 'middle');
    col1.text(colName1);
    row.append(col1);

    var col2 = j('<div></div>');
    col2.css('display', 'table-cell');
    col2.css('width', '30%');
    col2.css('vertical-align', 'middle');
    col2.text(colName2);
    row.append(col2);

    var col3 = j('<div></div>');
    col3.css('display', 'table-cell');
    col3.css('width', '30%');
    col3.css('vertical-align', 'middle');
    row.append(col3);
    if (isHeader) {
        col3.text(colName3);
    } else {
        var updateLink = j('<a external></a>');
        updateLink.css('margin-right', '5px');
        col3.append(updateLink);
        updateLink.text(colName3['update']);
        updateLink.attr('href', '../htmls/product_update.html?product-id=' + productId + '&shop-id=' + shopId);

        var enableLink = j('<a></a>');
        col3.append(enableLink);
        enableLink.text(colName3['enable']);
        enableLink.data('product-id', productId);
        enableLink.click(enableLinkOnClick);

        col3.append(j('<br/>'));

        var previewLink = j('<a></a>');
        previewLink.css('margin-right', '5px');
        previewLink.attr('class', 'external');
        col3.append(previewLink);
        previewLink.text(colName3['preview']);
        previewLink.attr('href', '../htmls/product_detail.html?preview=true&product-id=' + productId);

        var deleteLink = j('<a></a>');
        col3.append(deleteLink);
        deleteLink.text(colName3['delete']);
        deleteLink.data('product-id', productId);
        deleteLink.click(deleteLinkOnClick);
    }

    return row;
}

function getProductCategory(productCategoryId) {
    if (productCategoryId == null) {
        return '无类别';
    }

    var categoryName = '';
    var getProductCategoryAjax = j.ajax({
        url: backendAddr + "/product-category/" + productCategoryId,
        type: 'get',
        contentType: 'application/json; charset=utf-8',
        timeout: 5000,
        async: false,
        headers: {
            Authorization: AUTHORIZATION_PREFIX + window.localStorage.jwt
        },
        success: function (data) {
            categoryName = data.name;
        },
        error: function (resp) {
            if (resp.statusText == 'timeout') {
                getProductCategoryAjax.abort();
                $.toast('系统繁忙');
                return;
            }

            if (resp.responseJSON.failed) {
                $.toast(resp.responseJSON.messages[0]);
            } else {
                $.toast('系统错误');
            }
        }
    });

    return categoryName;
}

if (window.localStorage.jwt == undefined || window.localStorage.jwt == null || window.localStorage.jwt == '') {
    $.toast('请先登录');
} else {
    var getProductListAjax = j.ajax({
        url: backendAddr + "/products",
        type: 'get',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        data: {page: currentPage, size: pageSize, 'shop-id': shopId},
        timeout: 5000,
        async: false,
        headers: {
            Authorization: AUTHORIZATION_PREFIX + window.localStorage.jwt
        },
        success: function (data) {
            if (data.list.length > 0) {
                j('#content').append(createRow('物品名称', '分类', '操作', true, null));

                var productList = j('<div id="product-list"></div>');
                productList.attr('class', 'shop-wrap');
                j('#content').append(productList);

                for (var i = 0; i < data.list.length; i++) {
                    productList.append(createRow(data.list[i].name, getProductCategory(data.list[i].categoryId), {
                        update: '编辑',
                        enable: data.list[i].available ? '下架' : '上架',
                        preview: '预览',
                        delete: '删除'
                    }, false, data.list[i].id));
                }

                j('#content').append(createPageBar());
                setPageBar(data);
                hasPrevPage = data.hasPreviousPage;
                hasNextPage = data.hasNextPage;
                currentPage = data.pageNum;
                totalPage = data.pages;
            } else {
                var emptyList = j('<div></div>');
                emptyList.css('color', 'gray');
                emptyList.css('width', '100%');
                emptyList.css('height', '100px');
                emptyList.css('text-align', 'center');
                j('#content').append(emptyList);

                var emptyTip = j('<p></p>');
                emptyTip.text('暂时还没有任何物品哦');
                emptyTip.css('height', '100px');
                emptyTip.css('line-height', '100px');
                emptyList.append(emptyTip);
            }
        },
        error: function (resp) {
            if (resp.statusText == 'timeout') {
                getProductListAjax.abort();
                $.toast('系统繁忙');
                return;
            }

            if (resp.responseJSON.failed) {
                $.toast(resp.responseJSON.messages[0]);
            } else {
                $.toast('系统错误');
            }
        }
    })
}

j('#prev-page').click(
    function () {
        if (!hasPrevPage) {
            return;
        }
        if (window.localStorage.jwt == undefined || window.localStorage.jwt == null || window.localStorage.jwt == '') {
            $.toast('请先登录');
            return;
        }

        currentPage--;
        var getShopListAjax = j.ajax({
            url: backendAddr + "/products",
            type: 'get',
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            data: {page: currentPage, size: pageSize, 'shop-id': shopId},
            timeout: 5000,
            headers: {
                Authorization: AUTHORIZATION_PREFIX + window.localStorage.jwt
            },
            success: function (data) {
                j('#product-list').children().remove();
                for (var i = 0; i < data.list.length; i++) {
                    j('#product-list').append(createRow(data.list[i].name, getProductCategory(data.list[i].categoryId), {
                        update: '编辑',
                        enable: data.list[i].available ? '下架' : '上架',
                        preview: '预览',
                        delete: '删除'
                    }, false, data.list[i].id));
                }

                setPageBar(data);
                hasPrevPage = data.hasPreviousPage;
                hasNextPage = data.hasNextPage;
                currentPage = data.pageNum;
                totalPage = data.pages;
            },
            error: function (resp) {
                if (resp.statusText == 'timeout') {
                    getShopListAjax.abort();
                    $.toast('系统繁忙');
                    return;
                }

                if (resp.responseJSON.failed) {
                    $.toast(resp.responseJSON.messages[0]);
                } else {
                    $.toast('系统错误');
                }
            }
        })
    }
);

j('#first-page').click(
    function () {
        if (!hasPrevPage) {
            return;
        }
        if (window.localStorage.jwt == undefined || window.localStorage.jwt == null || window.localStorage.jwt == '') {
            $.toast('请先登录');
            return;
        }

        currentPage = 1;
        var getShopListAjax = j.ajax({
            url: backendAddr + "/products",
            type: 'get',
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            data: {page: currentPage, size: pageSize, 'shop-id': shopId},
            timeout: 5000,
            headers: {
                Authorization: AUTHORIZATION_PREFIX + window.localStorage.jwt
            },
            success: function (data) {
                j('#product-list').children().remove();
                for (var i = 0; i < data.list.length; i++) {
                    j('#product-list').append(createRow(data.list[i].name, getProductCategory(data.list[i].categoryId), {
                        update: '编辑',
                        enable: data.list[i].available ? '下架' : '上架',
                        preview: '预览',
                        delete: '删除'
                    }, false, data.list[i].id));
                }

                setPageBar(data);
                hasPrevPage = data.hasPreviousPage;
                hasNextPage = data.hasNextPage;
                currentPage = data.pageNum;
                totalPage = data.pages;
            },
            error: function (resp) {
                if (resp.statusText == 'timeout') {
                    getShopListAjax.abort();
                    $.toast('系统繁忙');
                    return;
                }

                if (resp.responseJSON.failed) {
                    $.toast(resp.responseJSON.messages[0]);
                } else {
                    $.toast('系统错误');
                }
            }
        })
    }
);

j('#next-page').click(
    function () {
        if (!hasNextPage) {
            return;
        }
        if (window.localStorage.jwt == undefined || window.localStorage.jwt == null || window.localStorage.jwt == '') {
            $.toast('请先登录');
            return;
        }

        currentPage++;
        var getShopListAjax = j.ajax({
            url: backendAddr + "/products",
            type: 'get',
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            data: {page: currentPage, size: pageSize, 'shop-id': shopId},
            timeout: 5000,
            headers: {
                Authorization: AUTHORIZATION_PREFIX + window.localStorage.jwt
            },
            success: function (data) {
                j('#product-list').children().remove();
                for (var i = 0; i < data.list.length; i++) {
                    j('#product-list').append(createRow(data.list[i].name, getProductCategory(data.list[i].categoryId), {
                        update: '编辑',
                        enable: data.list[i].available ? '下架' : '上架',
                        preview: '预览',
                        delete: '删除'
                    }, false, data.list[i].id));
                }

                setPageBar(data);
                hasPrevPage = data.hasPreviousPage;
                hasNextPage = data.hasNextPage;
                currentPage = data.pageNum;
                totalPage = data.pages;
            },
            error: function (resp) {
                if (resp.statusText == 'timeout') {
                    getShopListAjax.abort();
                    $.toast('系统繁忙');
                    return;
                }

                if (resp.responseJSON.failed) {
                    $.toast(resp.responseJSON.messages[0]);
                } else {
                    $.toast('系统错误');
                }
            }
        })
    }
);

j('#last-page').click(
    function () {
        if (!hasNextPage) {
            return;
        }
        if (window.localStorage.jwt == undefined || window.localStorage.jwt == null || window.localStorage.jwt == '') {
            $.toast('请先登录');
            return;
        }

        currentPage = totalPage;
        var getShopListAjax = j.ajax({
            url: backendAddr + "/products",
            type: 'get',
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            data: {page: currentPage, size: pageSize, 'shop-id': shopId},
            timeout: 5000,
            headers: {
                Authorization: AUTHORIZATION_PREFIX + window.localStorage.jwt
            },
            success: function (data) {
                j('#product-list').children().remove();
                for (var i = 0; i < data.list.length; i++) {
                    j('#product-list').append(createRow(data.list[i].name, getProductCategory(data.list[i].categoryId), {
                        update: '编辑',
                        enable: data.list[i].available ? '下架' : '上架',
                        preview: '预览',
                        delete: '删除'
                    }, false, data.list[i].id));
                }

                setPageBar(data);
                hasPrevPage = data.hasPreviousPage;
                hasNextPage = data.hasNextPage;
                currentPage = data.pageNum;
                totalPage = data.pages;
            },
            error: function (resp) {
                if (resp.statusText == 'timeout') {
                    getShopListAjax.abort();
                    $.toast('系统繁忙');
                    return;
                }

                if (resp.responseJSON.failed) {
                    $.toast(resp.responseJSON.messages[0]);
                } else {
                    $.toast('系统错误');
                }
            }
        })
    }
);

if (window.localStorage.from == 'product_create') {
    $.toast('添加成功');
    window.localStorage.from = null;
} else if (window.localStorage.from == 'delete_product') {
    $.toast('删除成功');
    window.localStorage.from = null;
} else if (window.localStorage.from == 'product_update') {
    $.toast('修改成功');
    window.localStorage.from = null;
}
