var hasPrevPage = false;
var hasNextPage = false;
var currentPage = 1;
var totalPage = 1;
var pageSize = 2;

var shopId = getShopIdFromHref();

var getShopInfoAjax = j.ajax({
    url: backendAddr + '/shop/' + shopId,
    type: 'get',
    contentType: 'application/json; charset=utf-8',
    timeout: 5000,
    success: function (data) {
        j('h1.title').text(data.name);
        j('title').text(data.name);
        j('#shop-update-time').text(timeStampToDate(data.lastUpdateTime) + '更新');
        if (data.description != null) {
            j('#shop-desc').text(data.description);
        } else {
            j('#shop-desc').text('描述暂无');
        }
        if (data.address != null) {
            j('#shop-addr').text(data.address);
        } else {
            j('#shop-addr').text('地址暂无');
        }
        if (data.phone != null) {
            j('#shop-phone').text(data.phone);
        } else {
            j('#shop-phone').text('联系方式暂无');
        }
        if (data.imageFileName == null) {
            j('#shop-cover-pic').attr('src', '../images/default.png');
            j('#shop-cover-pic').attr('style', 'padding: 32px 128px');
        } else {
            j('#shop-cover-pic').removeAttr('style');
            j('#shop-cover-pic').attr('src', backendAddr + '/image/shop/' + data.imageFileName);
        }
    },
    error: function (resp) {
        if (resp.statusText == 'timeout') {
            getShopInfoAjax.abort();
            $.toast('系统繁忙');
            return;
        }

        if (resp.responseJSON.failed) {
            $.toast(resp.responseJSON.messages[0]);
        } else {
            $.toast('系统错误');
        }
    }
});

var getProductCategoriesAjax = j.ajax({
    url: backendAddr + '/product-categories?shop-id=' + shopId,
    type: 'get',
    contentType: 'application/json; charset=utf-8',
    timeout: 5000,
    success: function (data) {
        for (var i = 0; i < data.length; i++) {
            var productCategory = j('<a class="button"></a>');
            productCategory.text(data[i].name);
            productCategory.attr('id', data[i].id);
            productCategory.click(switchCategory);
            j('#product-categories-div').append(productCategory);
        }
    },
    error: function (resp) {
        if (resp.statusText == 'timeout') {
            getProductCategoriesAjax.abort();
            $.toast('系统繁忙');
            return;
        }

        if (resp.responseJSON.failed) {
            $.toast(resp.responseJSON.messages[0]);
        } else {
            $.toast('系统错误');
        }
    }
});

var getProductsAjax = j.ajax({
    url: backendAddr + '/products?page=' + currentPage + '&size=' + pageSize + '&shop-id=' + shopId,
    type: 'get',
    contentType: 'application/json; charset=utf-8',
    timeout: 5000,
    async: false,
    success: function (data) {
        if (data.list.length == 0) {
            j('#product-list').append(createEmptyTip('暂时还没有任何物品哦'));
            return;
        }

        for (var i = 0; i < data.list.length; i++) {
            createProductCard(data.list[i]);
        }

        setPageBar(data);
        hasPrevPage = data.hasPreviousPage;
        hasNextPage = data.hasNextPage;
        currentPage = data.pageNum;
        totalPage = data.pages;
    },
    error: function (resp) {
        if (resp.statusText == 'timeout') {
            getProductsAjax.abort();
            $.toast('系统繁忙');
            return;
        }

        if (resp.responseJSON.failed) {
            $.toast(resp.responseJSON.messages[0]);
        } else {
            $.toast('系统错误');
        }
    }
});

j('#prev-page').click(
    function () {
        if (!hasPrevPage) {
            return;
        }

        currentPage--;
        var url = backendAddr + '/products?page=' + currentPage + '&size=' + pageSize + '&shop-id=' + shopId;
        var productCategories = j('#product-categories-div a');
        for (var i = 0; i < productCategories.length; i++) {
            if (j(productCategories[i]).attr('style') == 'background-color: cornflowerblue; color: white;') {
                if (j(productCategories[i]).attr('id') != 'all') {
                    url += '&category-id=' + j(productCategories[i]).attr('id');
                }
                break;
            }
        }

        var getProductListAjax = j.ajax({
            url: url,
            type: 'get',
            contentType: 'application/json; charset=utf-8',
            timeout: 5000,
            success: function (data) {
                j('#product-list').children().remove();
                for (var i = 0; i < data.list.length; i++) {
                    createProductCard(data.list[i]);
                }

                setPageBar(data);
                hasPrevPage = data.hasPreviousPage;
                hasNextPage = data.hasNextPage;
                currentPage = data.pageNum;
                totalPage = data.pages;
            },
            error: function (resp) {
                if (resp.statusText == 'timeout') {
                    getProductListAjax.abort();
                    $.toast('系统繁忙');
                    return;
                }

                if (resp.responseJSON.failed) {
                    $.toast(resp.responseJSON.messages[0]);
                } else {
                    $.toast('系统错误');
                }
            }
        })
    }
);

j('#first-page').click(
    function () {
        if (!hasPrevPage) {
            return;
        }

        currentPage = 1;
        var url = backendAddr + '/products?page=' + currentPage + '&size=' + pageSize + '&shop-id=' + shopId;
        var productCategories = j('#product-categories-div a');
        for (var i = 0; i < productCategories.length; i++) {
            if (j(productCategories[i]).attr('style') == 'background-color: cornflowerblue; color: white;') {
                if (j(productCategories[i]).attr('id') != 'all') {
                    url += '&category-id=' + j(productCategories[i]).attr('id');
                }
                break;
            }
        }

        var getProductListAjax = j.ajax({
            url: url,
            type: 'get',
            contentType: 'application/json; charset=utf-8',
            timeout: 5000,
            success: function (data) {
                j('#product-list').children().remove();
                for (var i = 0; i < data.list.length; i++) {
                    createProductCard(data.list[i]);
                }

                setPageBar(data);
                hasPrevPage = data.hasPreviousPage;
                hasNextPage = data.hasNextPage;
                currentPage = data.pageNum;
                totalPage = data.pages;
            },
            error: function (resp) {
                if (resp.statusText == 'timeout') {
                    getProductListAjax.abort();
                    $.toast('系统繁忙');
                    return;
                }

                if (resp.responseJSON.failed) {
                    $.toast(resp.responseJSON.messages[0]);
                } else {
                    $.toast('系统错误');
                }
            }
        })
    }
);

j('#next-page').click(
    function () {
        if (!hasNextPage) {
            return;
        }

        currentPage++;
        var url = backendAddr + '/products?page=' + currentPage + '&size=' + pageSize + '&shop-id=' + shopId;
        var productCategories = j('#product-categories-div a');
        for (var i = 0; i < productCategories.length; i++) {
            if (j(productCategories[i]).attr('style') == 'background-color: cornflowerblue; color: white;') {
                if (j(productCategories[i]).attr('id') != 'all') {
                    url += '&category-id=' + j(productCategories[i]).attr('id');
                }
                break;
            }
        }

        var getProductListAjax = j.ajax({
            url: url,
            type: 'get',
            contentType: 'application/json; charset=utf-8',
            timeout: 5000,
            success: function (data) {
                j('#product-list').children().remove();
                for (var i = 0; i < data.list.length; i++) {
                    createProductCard(data.list[i]);
                }

                setPageBar(data);
                hasPrevPage = data.hasPreviousPage;
                hasNextPage = data.hasNextPage;
                currentPage = data.pageNum;
                totalPage = data.pages;
            },
            error: function (resp) {
                if (resp.statusText == 'timeout') {
                    getProductListAjax.abort();
                    $.toast('系统繁忙');
                    return;
                }

                if (resp.responseJSON.failed) {
                    $.toast(resp.responseJSON.messages[0]);
                } else {
                    $.toast('系统错误');
                }
            }
        })
    }
);

j('#last-page').click(
    function () {
        if (!hasNextPage) {
            return;
        }

        currentPage = totalPage;
        var url = backendAddr + '/products?page=' + currentPage + '&size=' + pageSize + '&shop-id=' + shopId;
        var productCategories = j('#product-categories-div a');
        for (var i = 0; i < productCategories.length; i++) {
            if (j(productCategories[i]).attr('style') == 'background-color: cornflowerblue; color: white;') {
                if (j(productCategories[i]).attr('id') != 'all') {
                    url += '&category-id=' + j(productCategories[i]).attr('id');
                }
                break;
            }
        }

        var getProductListAjax = j.ajax({
            url: url,
            type: 'get',
            contentType: 'application/json; charset=utf-8',
            timeout: 5000,
            success: function (data) {
                j('#product-list').children().remove();
                for (var i = 0; i < data.list.length; i++) {
                    createProductCard(data.list[i]);
                }

                setPageBar(data);
                hasPrevPage = data.hasPreviousPage;
                hasNextPage = data.hasNextPage;
                currentPage = data.pageNum;
                totalPage = data.pages;
            },
            error: function (resp) {
                if (resp.statusText == 'timeout') {
                    getProductListAjax.abort();
                    $.toast('系统繁忙');
                    return;
                }

                if (resp.responseJSON.failed) {
                    $.toast(resp.responseJSON.messages[0]);
                } else {
                    $.toast('系统错误');
                }
            }
        })
    }
);

j('#me').click(
    function () {
        $.openPanel('#right-panel');
    }
);

if (!(window.localStorage.jwt == undefined || window.localStorage.jwt == null || window.localStorage.jwt == '')) {
    j('#log-in-out').text('退出登录');
    j('#log-in-out').click(
        function () {
            window.localStorage.removeItem('jwt');
        }
    );
    j('#log-in-out').attr('href', './index.html');
    j('#register').hide();
    j('#modify-pwd').show();
    j('#edit-user-info').show();
    j('#my-shops').show();
    j('#my-message').show();
    var getUserInfoAjax = j.ajax({
        url: backendAddr + "/user",
        type: 'get',
        contentType: 'application/json; charset=utf-8',
        timeout: 5000,
        headers: {
            Authorization: AUTHORIZATION_PREFIX + window.localStorage.jwt
        },
        success: function (data) {
            j('#hi').text('hi, ' + data.nickname);
        },
        error: function (resp) {
            if (resp.statusText == 'timeout') {
                getUserInfoAjax.abort();
                $.toast('系统繁忙');
                return;
            }

            if (resp.responseJSON.failed) {
                $.toast(resp.responseJSON.messages[0]);
            } else {
                $.toast('系统错误');
            }
        }
    });

} else {
    j('#log-in-out').text('登录');
    j('#log-in-out').unbind();
    j('#log-in-out').click(
        function () {
            window.localStorage.from = window.location.href;
        }
    );
    j('#log-in-out').attr('href', '../htmls/sign_in.html');
    j('#register').show();
    j('#modify-pwd').hide();
    j('#edit-user-info').hide();
    j('#my-shops').hide();
    j('#my-message').hide();
    j('#hi').text('hi');
}

j('#register').click(
    function () {
        window.localStorage.from = window.location.href;
    }
);

j('#modify-pwd').click(
    function () {
        window.localStorage.from = window.location.href;
    }
);

j('#edit-user-info').click(function () {
    window.localStorage.from = window.location.href;
});

function switchCategory() {
    var buttons = j('#product-categories-div>a');
    for (var i = 0; i < buttons.length; i++) {
        if (buttons[i] != j(this)[0]) {
            j(buttons[i]).removeAttr('style');
        }
    }
    j(this).attr('style', 'background-color: cornflowerblue; color: white;');

    hasPrevPage = false;
    hasNextPage = false;
    currentPage = 1;
    totalPage = 1;
    pageSize = 2;

    var url = backendAddr + '/products?page=' + currentPage + '&size=' + pageSize + '&shop-id=' + shopId;
    if (j(this).attr('id') != 'all') {
        url += '&category-id=' + j(this).attr('id');
    }

    var getProductListAjax = j.ajax({
        url: url,
        type: 'get',
        contentType: 'application/json; charset=utf-8',
        timeout: 5000,
        success: function (data) {
            j('#product-list').children().remove();
            if (data.list.length == 0) {
                hidePageBar();
                j('#product-list').append(createEmptyTip('暂时还没有任何物品哦'));
                return;
            }

            for (var i = 0; i < data.list.length; i++) {
                createProductCard(data.list[i]);
            }

            setPageBar(data);
            hasPrevPage = data.hasPreviousPage;
            hasNextPage = data.hasNextPage;
            currentPage = data.pageNum;
            totalPage = data.pages;
        },
        error: function (resp) {
            if (resp.statusText == 'timeout') {
                getProductListAjax.abort();
                $.toast('系统繁忙');
                return;
            }

            if (resp.responseJSON.failed) {
                $.toast(resp.responseJSON.messages[0]);
            } else {
                $.toast('系统错误');
            }
        }
    })
}

j('#product-categories-div>a').click(switchCategory);

j('#my-message a').click(function () {
    window.localStorage.from = window.location.href;
    window.location.href = '../htmls/my_messages.html';
});

if (window.localStorage.from == 'pwd_update') {
    $.toast('修改成功');
    window.localStorage.from = null;
}

if (window.localStorage.from == 'register') {
    $.toast('注册成功');
    window.localStorage.from = null;
}

if (window.localStorage.from == 'user_update') {
    $.toast('修改成功');
    window.localStorage.from = null;
}
