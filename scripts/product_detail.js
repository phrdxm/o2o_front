var productId = getProductIdFromHref();
var shopId = null;
var ownerId = null;

var isPreview = window.location.href.indexOf('preview=true') != -1;

var getProductInfoAjax = j.ajax({
    url: backendAddr + '/product/' + productId,
    type: 'get',
    contentType: 'application/json; charset=utf-8',
    timeout: 5000,
    async: false,
    success: function (data) {
        if (isPreview) {
            j('h1.title').text(data.name + ' 预览');
            j('title').text(data.name + ' 预览');
        } else {
            j('h1.title').text(data.name);
            j('title').text(data.name);
        }
        j('#product-last-update-time').text(timeStampToDate(data.lastUpdateTime) + '更新');
        if (data.description != null) {
            j('#product-desc').text(data.description);
        } else {
            j('#product-desc').text('描述暂无');
        }
        if (data.imageFileName == null) {
            j('#product-img').attr('src', '../images/default.png');
            j('#product-img').attr('style', 'padding: 32px 128px');
        } else {
            j('#product-img').removeAttr('style');
            j('#product-img').attr('src', backendAddr + '/image/product/' + data.imageFileName);
        }
        if (data.promotionPrice != null) {
            var normalPrice = j('<span class="normal-price" id="normal-price"></span>');
            normalPrice.text(data.normalPrice + '¥');
            j('#price').append(normalPrice);

            j('#price').append(j('<span><font color="red" size="4" id="promotion-price"></font></span>'));
            j('#promotion-price').text(data.promotionPrice + '¥');
        } else {
            j('#price').append(j('<span><font color="red" size="4" id="normal-price"></font></span>'));
            j('#normal-price').text(data.normalPrice + '¥');
        }
        j('#number').text('剩余数量：' + data.number + '件');
        shopId = data.shopId;
    },
    error: function (resp) {
        if (resp.statusText == 'timeout') {
            getProductInfoAjax.abort();
            $.toast('系统繁忙');
            return;
        }

        if (resp.responseJSON.failed) {
            $.toast(resp.responseJSON.messages[0]);
        } else {
            $.toast('系统错误');
        }
    }
});

if (shopId != null && !isPreview) {
    var getOwnerInfoAjax = j.ajax({
        url: backendAddr + '/shop/' + shopId + '/owner',
        type: 'get',
        contentType: 'application/json; charset=utf-8',
        timeout: 5000,
        async: false,
        success: function (data) {
            ownerId = data.id;
        },
        error: function (resp) {
            if (resp.statusText == 'timeout') {
                getOwnerInfoAjax.abort();
                $.toast('系统繁忙');
                return;
            }

            if (resp.responseJSON.failed) {
                $.toast(resp.responseJSON.messages[0]);
            } else {
                $.toast('系统错误');
            }
        }
    });
}

var getProductDetailImagesAjax = j.ajax({
    url: backendAddr + '/product-images?product-id=' + productId,
    type: 'get',
    contentType: 'application/json; charset=utf-8',
    timeout: 5000,
    success: function (data) {
        if (data.length > 0) {
            var imgList = j('<div class="card-header color-white no-border no-padding" style="display: block" id="img-list"></div>');
            j('#product-card').append(imgList);
            for (var i = 0; i < data.length; i++) {
                var imgDiv = j('<div><img class="card-cover"/></div>');
                imgList.append(imgDiv);
                var img = imgDiv.find('img');
                img.attr('src', backendAddr + '/image/product-detail/' + data[i].fileName);
                if (data[i].description != null) {
                    img.attr('alt', data[i].description);
                }
            }
        }
    },
    error: function (resp) {
        if (resp.statusText == 'timeout') {
            getProductDetailImagesAjax.abort();
            $.toast('系统繁忙');
            return;
        }

        if (resp.responseJSON.failed) {
            $.toast(resp.responseJSON.messages[0]);
        } else {
            $.toast('系统错误');
        }
    }
});

j('#me').click(
    function () {
        $.openPanel('#right-panel');
    }
);

if (!(window.localStorage.jwt == undefined || window.localStorage.jwt == null || window.localStorage.jwt == '')) {
    j('#log-in-out').text('退出登录');
    j('#log-in-out').click(
        function () {
            window.localStorage.removeItem('jwt');
        }
    );
    j('#log-in-out').attr('href', './index.html');
    j('#register').hide();
    j('#modify-pwd').show();
    j('#edit-user-info').show();
    j('#my-shops').show();
    j('#my-message').show();
    var getUserInfoAjax = j.ajax({
        url: backendAddr + "/user",
        type: 'get',
        contentType: 'application/json; charset=utf-8',
        timeout: 5000,
        headers: {
            Authorization: AUTHORIZATION_PREFIX + window.localStorage.jwt
        },
        success: function (data) {
            j('#hi').text('hi, ' + data.nickname);
            if (!isPreview && data.id != ownerId) {
                j('#talk-now').attr('href', '../htmls/chat.html?owner-id=' + ownerId);
                j('#talk-now').removeAttr('style');
            }
        },
        error: function (resp) {
            if (resp.statusText == 'timeout') {
                getUserInfoAjax.abort();
                $.toast('系统繁忙');
                return;
            }

            if (resp.responseJSON.failed) {
                $.toast(resp.responseJSON.messages[0]);
            } else {
                $.toast('系统错误');
            }
        }
    });

} else {
    j('#log-in-out').text('登录');
    j('#log-in-out').unbind();
    j('#log-in-out').click(
        function () {
            window.localStorage.from = window.location.href;
        }
    );
    j('#log-in-out').attr('href', '../htmls/sign_in.html');
    j('#register').show();
    j('#talk-now').show();
    j('#talk-now').click(function () {
        $.toast('请先登录');
    });
    j('#modify-pwd').hide();
    j('#edit-user-info').hide();
    j('#my-shops').hide();
    j('#my-message').hide();
    j('#hi').text('hi');
}

j('#register').click(
    function () {
        window.localStorage.from = window.location.href;
    }
);

j('#modify-pwd').click(
    function () {
        window.localStorage.from = window.location.href;
    }
);

j('#edit-user-info').click(function () {
    window.localStorage.from = window.location.href;
});

j('#my-message a').click(function () {
    window.localStorage.from = window.location.href;
    window.location.href = '../htmls/my_messages.html';
});


if (window.localStorage.from == 'pwd_update') {
    $.toast('修改成功');
    window.localStorage.from = null;
}

if (window.localStorage.from == 'register') {
    $.toast('注册成功');
    window.localStorage.from = null;
}

if (window.localStorage.from == 'user_update') {
    $.toast('修改成功');
    window.localStorage.from = null;
}
