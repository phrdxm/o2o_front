appendShopId('back');

var captcha = {
    token: null,
    code: null
};

getCaptcha(captcha);

var shopId = getShopIdFromHref();

var formData = {
    name: null,
    description: null,
    normalPrice: null,
    promotionPrice: null,
    number: 1,
    available: true,
    categoryId: null,
    shopId: shopId,
    base64Image: null
};

var detailBase64Images = [];

function isValidated() {
    var condition1 = formData.name != null && formData.normalPrice != null &&
        formData.base64Image != FORM_ERR_SIGNAL && formData.promotionPrice != FORM_ERR_SIGNAL &&
        captcha.code != null && captcha.token != null;
    var condition2 = true;
    for (var i = 0; i < detailBase64Images.length; i++) {
        if (detailBase64Images[i] == FORM_ERR_SIGNAL) {
            condition2 = false;
            break;
        }
    }

    return condition1 && condition2;
}

if (window.localStorage.jwt == undefined || window.localStorage.jwt == null || window.localStorage.jwt == '') {
    $.toast('请先登录');
} else {
    var getProductCategoriesAjax = j.ajax({
        url: backendAddr + "/product-categories",
        type: 'get',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        data: {'shop-id': shopId},
        timeout: 5000,
        headers: {
            Authorization: AUTHORIZATION_PREFIX + window.localStorage.jwt
        },
        success: function (data) {
            var productCategoryOptions = j('#product-category-options');
            data.sort(function (a, b) {
                return b.lastUpdateTime - a.lastUpdateTime
            });
            for (var i = 0; i < data.length; i++) {
                var option = j('<option></option>');
                option.val(data[i].id);
                option.text(data[i].name);
                productCategoryOptions.append(option);
            }
        },
        error: function (resp) {
            if (resp.statusText == 'timeout') {
                getProductCategoriesAjax.abort();
                $.toast('系统繁忙');
                return;
            }

            if (resp.responseJSON.failed) {
                $.toast(resp.responseJSON.messages[0]);
            } else {
                $.toast('系统错误');
            }
        }
    })
}

j('#product-name').blur(
    function () {
        var productName = trim(j('#product-name').val());
        j('#product-name').val(productName);
        if (productName == '') {
            j('#product-name-tip .err-msg').text('物品名称不能为空');
            j('#product-name-tip').show();
            j('#submit').css('background-color', 'gray');
            formData.name = null;
        } else {
            j('#product-name-tip').hide();
            formData.name = productName;
            if (isValidated()) {
                j('#submit').attr('style', '');
            }
        }
    }
);

j('#product-category-options').change(
    function () {
        if (j('#product-category-options option:selected').val() == 'none') {
            formData.categoryId = null;
        } else {
            formData.categoryId = j('#product-category-options option:selected').val();
        }
    }
);

j('#normal-price').blur(
    function () {
        var normalPriceStr = trim(j('#normal-price').val());
        j('#normal-price').val(normalPriceStr);
        var normalPrice = Number(normalPriceStr);
        if (isNaN(normalPrice) || normalPriceStr == '') {
            j('#normal-price-tip .err-msg').text('请输入数值');
            j('#normal-price-tip').show();
            j('#submit').css('background-color', 'gray');
            formData.normalPrice = null;
        } else if (normalPrice < 0 || normalPrice > 99999.99) {
            j('#normal-price-tip .err-msg').text('数值范围0-99999.99');
            j('#normal-price-tip').show();
            j('#submit').css('background-color', 'gray');
            formData.normalPrice = null;
        } else {
            j('#normal-price-tip').hide();
            formData.normalPrice = normalPrice;
            if (isValidated()) {
                j('#submit').attr('style', '');
            }
        }
    }
);

j('#promotion-price').blur(
    function () {
        var promotionPriceStr = trim(j('#promotion-price').val());
        j('#promotion-price').val(promotionPriceStr);
        if (promotionPriceStr == '') {
            j('#promotion-price-tip').hide();
            formData.promotionPrice = null;
            if (isValidated()) {
                j('#submit').attr('style', '');
            }
            return;
        }
        var promotionPrice = Number(promotionPriceStr);
        if (isNaN(promotionPrice)) {
            j('#promotion-price-tip .err-msg').text('请输入数值');
            j('#promotion-price-tip').show();
            j('#submit').css('background-color', 'gray');
            formData.promotionPrice = FORM_ERR_SIGNAL;
        } else if (promotionPrice < 0 || promotionPrice > 99999.99) {
            j('#promotion-price-tip .err-msg').text('数值范围0-99999.99');
            j('#promotion-price-tip').show();
            j('#submit').css('background-color', 'gray');
            formData.promotionPrice = FORM_ERR_SIGNAL;
        } else {
            j('#promotion-price-tip').hide();
            formData.promotionPrice = promotionPrice;
            if (isValidated()) {
                j('#submit').attr('style', '');
            }
        }
    }
);

j('#number').blur(
    function () {
        var numberStr = trim(j('#number').val());
        if (numberStr == '') {
            j('#number').val(1);
            j('#number-tip').hide();
            formData.number = 1;
            if (isValidated()) {
                j('#submit').attr('style', '');
            }
            return;
        }
        var number = parseInt(numberStr);
        if (number < 1 || number > 9999) {
            j('#number').val(number);
            j('#number-tip .err-msg').text('数值范围1-9999');
            j('#number-tip').show();
            j('#submit').css('background-color', 'gray');
            formData.number = FORM_ERR_SIGNAL;
        } else {
            j('#number').val(number);
            j('#number-tip').hide();
            formData.number = number;
            if (isValidated()) {
                j('#submit').attr('style', '');
            }
        }
    }
);

j('#available-checkbox').click(
    function () {
        formData.available = !formData.available;
    }
);

j('#product-img').change(
    function () {
        var $file = $(this);
        var fileObj = $file[0];
        var dataURL;

        if (fileObj && fileObj.files && fileObj.files[0]) {
            dataURL = (window.URL || window.webkitURL).createObjectURL(fileObj.files[0]);
        } else {
            dataURL = $file.val();
        }

        if (dataURL == '') {
            j('#product-img-tip').hide();
            formData.base64Image = null;
            if (isValidated()) {
                j('#submit').attr('style', '');
            }
        } else {
            var image = new Image();
            image.src = dataURL;
            image.onload = function () {
                var base64Code = getBase64Image(image).replace(/^data:[a-zA-Z/*]+;base64,/, '');
                if (base64Code.length > 1320000) {
                    j('#product-img-tip .err-msg').text('上传文件最大1MB');
                    j('#product-img-tip').show();
                    formData.base64Image = FORM_ERR_SIGNAL;
                    j('#submit').css('background-color', 'gray');
                } else {
                    formData.base64Image = base64Code;
                    j('#product-img-tip').hide();
                    if (isValidated()) {
                        j('#submit').attr('style', '');
                    }
                }
            };
            image.onerror = function () {
                j('#product-img-tip .err-msg').text('上传文件类型错误，请上传.jpg，.jpeg或.png格式的文件');
                j('#product-img-tip').show();
                formData.base64Image = FORM_ERR_SIGNAL;
                j('#submit').css('background-color', 'gray');
            }
        }
    }
);

var counter = 2;

function detailImgOnChange() {
    var $file = $(this);
    var fileObj = $file[0];
    var dataURL;

    if (fileObj && fileObj.files && fileObj.files[0]) {
        dataURL = (window.URL || window.webkitURL).createObjectURL(fileObj.files[0]);
    } else {
        dataURL = $file.val();
    }


    var detailImgInput = j(this);
    var detailImg = j(detailImgInput.parents('li')[0]);
    var detailImgTip = detailImg.next();

    detailImgInput.data("old", detailImgInput.data("new") || "");
    detailImgInput.data("new", detailImgInput.val());

    if (dataURL == '') {
        if (j('.detail-img').length > 1) {
            detailImgTip.remove();
            detailImg.remove();
            j('.detail-img:first').parent().prev().text('详情图片');
        } else {
            detailImgTip.find('.form-tip').hide();
        }
        detailBase64Images[parseInt(detailImgInput.attr('id'))] = undefined;
        if (isValidated()) {
            j('#submit').attr('style', '');
        }
    } else {
        var image = new Image();
        image.src = dataURL;
        image.onload = function () {
            var base64Code = getBase64Image(image).replace(/^data:[a-zA-Z/*]+;base64,/, '');
            if (base64Code.length > 1320000) {
                detailImgTip.find('.err-msg').text('上传文件最大1MB');
                detailImgTip.find('.form-tip').show();
                detailBase64Images[parseInt(detailImgInput.attr('id'))] = FORM_ERR_SIGNAL;
                j('#submit').css('background-color', 'gray');
            } else {
                detailBase64Images[parseInt(detailImgInput.attr('id'))] = base64Code;
                detailImgTip.find('.form-tip').hide();
                if (isValidated()) {
                    j('#submit').attr('style', '');
                }
            }
        };
        image.onerror = function () {
            detailImgTip.find('.err-msg').text('上传文件类型错误，请上传.jpg，.jpeg或.png格式的文件');
            detailImgTip.find('.form-tip').show();
            detailBase64Images[parseInt(detailImgInput.attr('id'))] = FORM_ERR_SIGNAL;
            j('#submit').css('background-color', 'gray');
        };

        if (detailImgInput.data('old') == '') {
            var newDetailImg = j('<li>' +
                '<div class="item-content">' +
                '<div class="item-inner">' +
                '<div class="item-title label"></div>' +
                '<div class="item-input">' +
                '<input type="file" id="' + counter + '" class="detail-img"/>' +
                '</div></div></div></li>');
            var newDetailImgTip = j('<li>' +
                '<div class="form-tip">' +
                '<table><tbody><tr><td>' +
                '<img src="../images/error.png"/>' +
                '</td>' +
                '<td class="err-msg">' +
                '</td></tr></tbody></table></div></li>');
            detailImgTip.after(newDetailImg);
            newDetailImg.after(newDetailImgTip);
            counter++;
            j('.detail-img').change(detailImgOnChange);
        }
    }
}

j('.detail-img').change(detailImgOnChange);

j('#product-desc').blur(
    function () {
        var productDesc = trim(j('#product-desc').val());
        j('#product-desc').val(productDesc);
        if (productDesc != '') {
            formData.description = productDesc;
        } else {
            formData.description = null;
        }
    }
);

j('#captcha-img').click(
    function () {
        getCaptcha(captcha);
    }
);

j('#captcha-code').blur(
    function () {
        var code = trim(j('#captcha-code').val());
        if (code == '') {
            j('#captcha-tip .err-msg').text('验证码不能为空');
            j('#captcha-tip').show();
            captcha.code = null;
            j('#submit').css('background-color', 'gray');
        } else {
            j('#captcha-tip').hide();
            captcha.code = code;
            if (isValidated()) {
                j('#submit').attr('style', '');
            }
        }
    }
);

j('#submit').click(
    function () {
        if (window.localStorage.jwt == undefined || window.localStorage.jwt == null || window.localStorage.jwt == '') {
            $.toast('请先登录');
            return;
        }
        if (!isValidated()) {
            return;
        }

        var productId = null;
        var addProductAjax = j.ajax({
            url: backendAddr + "/product",
            type: 'post',
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            data: JSON.stringify(formData),
            timeout: 5000,
            async: false,
            headers: {
                Captcha: captcha.token + '=' + captcha.code,
                Authorization: AUTHORIZATION_PREFIX + window.localStorage.jwt
            },
            success: function (data) {
                productId = data.id;
                window.localStorage.from = 'product_create';
                window.location.href = '../htmls/product_list.html?shop-id=' + shopId;
            },
            error: function (resp) {
                if (resp.statusText == 'timeout') {
                    addProductAjax.abort();
                    $.toast('系统繁忙');
                    return;
                }

                if (resp.responseJSON.failed) {
                    $.toast(resp.responseJSON.messages[0]);
                } else {
                    $.toast('系统错误');
                }
            }
        });

        if (productId == null) {
            return;
        }

        for (var i = 0; i < detailBase64Images.length; i++) {
            if (detailBase64Images[i] != undefined && detailBase64Images[i] != null && detailBase64Images[i] != FORM_ERR_SIGNAL) {
                var addProductAjax = j.ajax({
                    url: backendAddr + "/product-image",
                    type: 'post',
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    data: JSON.stringify({productId: productId, base64Image: detailBase64Images[i], description: formData.name}),
                    timeout: 5000,
                    async: false,
                    headers: {
                        Authorization: AUTHORIZATION_PREFIX + window.localStorage.jwt
                    },
                    error: function (resp) {
                        if (resp.statusText == 'timeout') {
                            addProductAjax.abort();
                            $.toast('系统繁忙');
                            return;
                        }

                        if (resp.responseJSON.failed) {
                            $.toast(resp.responseJSON.messages[0]);
                        } else {
                            $.toast('系统错误');
                        }
                    }
                })
            }
        }
    }
);

