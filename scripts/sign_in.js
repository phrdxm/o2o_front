var captcha = {
    token: null,
    code: null
};

getCaptcha(captcha);

var formData = {
    username: null,
    password: null
};

function isValidated() {
    return formData.username != null && formData.password != null &&
        captcha.token != null && captcha.code != null;
}

j('#username').blur(
    function () {
        var username = trim(j('#username').val());
        j('#username').val(username);
        if (username == '') {
            j('#username-tip .err-msg').text('用户名不能为空');
            j('#username-tip').show();
            formData.username = null;
            j('#submit').css('background-color', 'gray');
        } else if (username.search(/^\w{6,16}$/) == -1) {
            j('#username-tip .err-msg').text('用户名长度为6到16个字符，只能由数字字母下划线组成');
            j('#username-tip').show();
            formData.username = null;
            j('#submit').css('background-color', 'gray');
        } else {
            j('#username-tip').hide();
            formData.username = username;
            if (isValidated()) {
                j('#submit').attr('style', '');
            }
        }
    }
);

j('#pwd').blur(
    function () {
        var pwd = j('#pwd').val();
        if (pwd == '') {
            j('#pwd-tip .err-msg').text('密码不能为空');
            j('#pwd-tip').show();
            formData.password = null;
            j('#submit').css('background-color', 'gray');
        } else if (pwd.search(/^(?:\w|[~`!@#$%^&*?,:;()\-.+={}\[\]]){6,}$/) == -1) {
            j('#pwd-tip .err-msg').text('密码至少6位，只能由数字字母下划线以及特殊字符~`!@#$%^&*?,:;()\\-.+={}[]组成');
            j('#pwd-tip').show();
            formData.password = null;
            j('#submit').css('background-color', 'gray');
        } else {
            j('#pwd-tip').hide();
            formData.password = pwd;
            if (isValidated()) {
                j('#submit').attr('style', '');
            }
        }
    }
);

j('#submit').click(
    function () {
        if (!isValidated()) {
            return;
        }
        if (captcha.code.length != 4) {
            $.toast('验证码错误');
            return;
        }
        var submitAjax = j.ajax({
            url: backendAddr + "/user/sign-in",
            type: 'post',
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            data: JSON.stringify(formData),
            timeout: 5000,
            headers: {
                Captcha: captcha.token + '=' + captcha.code
            },
            success: function (data) {
                window.localStorage.jwt = data.jwt;
                var from = window.localStorage.from;
                window.localStorage.removeItem('from');
                window.location.href = from;
            },
            error: function (resp) {
                captcha.code = null;
                getCaptcha(captcha);
                j('#captcha-code').val('');
                j('#submit').css('background-color', 'gray');

                if (resp.statusText == 'timeout') {
                    submitAjax.abort();
                    $.toast('系统繁忙');
                    return;
                }

                if (resp.responseJSON.failed) {
                    $.toast(resp.responseJSON.messages[0])
                } else {
                    $.toast('系统错误');
                }
            }
        })
    }
);

j('#cancel').click(
    function () {
        var from = window.localStorage.from;
        window.localStorage.removeItem('from');
        window.location.href = from;
    }
);

j('#captcha-img').click(
    function () {
        getCaptcha(captcha);
    }
);

j('#captcha-code').blur(
    function () {
        var code = trim(j('#captcha-code').val());
        if (code == '') {
            j('#captcha-tip .err-msg').text('验证码不能为空');
            j('#captcha-tip').show();
            captcha.code = null;
            j('#submit').css('background-color', 'gray');
        } else {
            j('#captcha-tip').hide();
            captcha.code = code;
            if (isValidated()) {
                j('#submit').attr('style', '');
            }
        }
    }
);
