appendShopId('shop-update');
appendShopId('product-category-manage');
appendShopId('product-list');

if (window.localStorage.from == 'shop_update') {
    $.toast('修改完成');
    window.localStorage.from = null;
}

function commitShop() {
    if (window.localStorage.jwt == undefined || window.localStorage.jwt == null || window.localStorage.jwt == '') {
        $.toast('请先登录');
        return;
    }
    var commitShopAjax = j.ajax({
        url: backendAddr + '/admin/shop/commit/' + getShopIdFromHref(),
        type: 'put',
        dataType: 'json',
        headers: {
            Authorization: AUTHORIZATION_PREFIX + window.localStorage.jwt
        },
        timeout: 5000,
        success: function () {
            $.toast('提交成功');
            j('#shop-status-manage').text('下架店铺');
            j('#shop-status-manage').click(disableShop);
        },
        error: function (resp) {
            if (resp.statusText == 'timeout') {
                commitShopAjax.abort();
                $.toast('系统繁忙');
                return;
            }

            if (resp.responseJSON.failed) {
                $.toast(resp.responseJSON.messages[0]);
            } else {
                $.toast('系统错误');
            }
        }
    });
}

function disableShop() {
    if (window.localStorage.jwt == undefined || window.localStorage.jwt == null || window.localStorage.jwt == '') {
        $.toast('请先登录');
        return;
    }
    var commitShopAjax = j.ajax({
        url: backendAddr + '/admin/shop/disable/' + getShopIdFromHref(),
        type: 'put',
        dataType: 'json',
        headers: {
            Authorization: AUTHORIZATION_PREFIX + window.localStorage.jwt
        },
        timeout: 5000,
        success: function () {
            $.toast('下架成功');
            j('#shop-status-manage').text('提交审核');
            j('#shop-status-manage').click(commitShop);
        },
        error: function (resp) {
            if (resp.statusText == 'timeout') {
                commitShopAjax.abort();
                $.toast('系统繁忙');
                return;
            }

            if (resp.responseJSON.failed) {
                $.toast(resp.responseJSON.messages[0]);
            } else {
                $.toast('系统错误');
            }
        }
    });
}

if (window.localStorage.jwt == undefined || window.localStorage.jwt == null || window.localStorage.jwt == '') {
    $.toast('请先登录');
} else {
    var getShopInfoAjax = j.ajax({
        url: backendAddr + '/shop/' + getShopIdFromHref(),
        type: 'get',
        dataType: 'json',
        headers: {
            Authorization: AUTHORIZATION_PREFIX + window.localStorage.jwt
        },
        timeout: 5000,
        async: false,
        success: function (data) {
            if (data.status == -1) {
                j('#shop-status-manage').text('提交审核');
                j('#shop-status-manage').click(commitShop);
            } else {
                j('#shop-status-manage').text('下架店铺');
                j('#shop-status-manage').click(disableShop);
            }
        },
        error: function (resp) {
            if (resp.statusText == 'timeout') {
                getShopInfoAjax.abort();
                $.toast('系统繁忙');
                return;
            }

            if (resp.responseJSON.failed) {
                $.toast(resp.responseJSON.messages[0]);
            } else {
                $.toast('系统错误');
            }
        }
    });
}

j('#delete-shop').click(
    function () {
        $.confirm('一旦删除，店铺中的所有内容都将无法恢复，你确定吗？', '删除', function () {
            $.modal({
                text: '请输入密码',
                afterText: '<input id="confirm-pwd" type="password" onkeyup="value=value.replace(/\\s/g,\'\')" onblur="window.localStorage.confirmPwd=value"/>',
                buttons: [
                    {
                        text: '取消',
                        onClick: function () {
                            window.localStorage.confirmPwd = null;
                        }
                    },
                    {
                        text: '确定',
                        onClick: function () {
                            if (window.localStorage.jwt == undefined || window.localStorage.jwt == null || window.localStorage.jwt == '') {
                                $.toast('请先登录');
                                window.localStorage.confirmPwd = null;
                                return;
                            }
                            if (window.localStorage.confirmPwd == null || window.localStorage.confirmPwd == undefined ||
                                window.localStorage.confirmPwd.search(/^(?:\w|[~`!@#$%^&*?,:;()\-.+={}\[\]]){6,}$/) == -1) {
                                $.toast('密码错误');
                                window.localStorage.confirmPwd = null;
                                return;
                            }
                            var pwd = window.localStorage.confirmPwd;
                            window.localStorage.confirmPwd = null;
                            var token = null;
                            var getAuthTokenAjax = j.ajax({
                                url: backendAddr + '/user/auth-token',
                                type: 'post',
                                contentType: 'application/json; charset=utf-8',
                                headers: {
                                    Authorization: AUTHORIZATION_PREFIX + window.localStorage.jwt
                                },
                                data: JSON.stringify({password: pwd}),
                                timeout: 5000,
                                async: false,
                                success: function (data) {
                                    token = data;
                                },
                                error: function (resp) {
                                    if (resp.statusText == 'timeout') {
                                        getAuthTokenAjax.abort();
                                        $.toast('系统繁忙');
                                        return;
                                    }

                                    if (resp.responseJSON.failed) {
                                        $.toast(resp.responseJSON.messages[0]);
                                    } else {
                                        $.toast('系统错误');
                                    }
                                }
                            });
                            if (token == null) {
                                return;
                            }
                            var deleteShopAjax = j.ajax({
                                url: backendAddr + '/shop/' + getShopIdFromHref() + '?token=' + token,
                                type: 'delete',
                                headers: {
                                    Authorization: AUTHORIZATION_PREFIX + window.localStorage.jwt
                                },
                                timeout: 5000,
                                success: function () {
                                    window.localStorage.from = 'delete_shop';
                                    window.location.href = '../htmls/shop_list.html';
                                },
                                error: function (resp) {
                                    if (resp.statusText == 'timeout') {
                                        deleteShopAjax.abort();
                                        $.toast('系统繁忙');
                                        return;
                                    }

                                    if (resp.responseJSON.failed) {
                                        $.toast(resp.responseJSON.messages[0]);
                                    } else {
                                        $.toast('系统错误');
                                    }
                                }
                            });
                        }
                    },
                ]
            })
        });
    }
);
