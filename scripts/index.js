var getPrimaryShopCategoryAjax = j.ajax({
    url: backendAddr + "/primary-categories",
    type: 'get',
    contentType: 'application/json; charset=utf-8',
    timeout: 5000,
    success: function (data) {
        for (var i = 0; i < data.length; i++) {
            var block = j('<div class="col-50 shop-classify" id="' + data[i].id + '">' +
                '<div class="word">' +
                '<p class="shop-title">' + data[i].name + '</p>' +
                '<p class="shop-desc">' + data[i].description + '</p></div>' +
                '<div class="shop-classify-img-warp">' +
                '<img class="shop-img" src="' + backendAddr + '/image/shop-category/' + data[i].imageFileName + '"/>' +
                '</div></div>');

            block.click(
                function () {
                    window.location.href = './htmls/shop_browse.html?category-id=' + j(this).attr('id');
                }
            );
            j('#primary-category').append(block);
        }
    },
    error: function (resp) {
        if (resp.statusText == 'timeout') {
            getPrimaryShopCategoryAjax.abort();
            $.toast('系统繁忙');
            return;
        }

        if (resp.responseJSON.failed) {
            $.toast(resp.responseJSON.messages[0]);
        } else {
            $.toast('系统错误');
        }
    }
});

j('#me').click(
    function () {
        $.openPanel('#right-panel');
    }
);

j('#my-message a').click(function () {
    window.localStorage.from = window.location.href;
    window.location.href = './htmls/my_messages.html';
});

if (!(window.localStorage.jwt == undefined || window.localStorage.jwt == null || window.localStorage.jwt == '')) {
    j('#log-in-out').text('退出登录');
    j('#log-in-out').click(
        function () {
            window.localStorage.removeItem('jwt');
        }
    );
    j('#log-in-out').attr('href', './index.html');
    j('#register').hide();
    j('#modify-pwd').show();
    j('#edit-user-info').show();
    j('#my-shops').show();
    j('#my-message').show();
    var getUserInfoAjax = j.ajax({
        url: backendAddr + "/user",
        type: 'get',
        contentType: 'application/json; charset=utf-8',
        timeout: 5000,
        headers: {
            Authorization: AUTHORIZATION_PREFIX + window.localStorage.jwt
        },
        success: function (data) {
            j('#hi').text('hi, ' + data.nickname);
        },
        error: function (resp) {
            if (resp.statusText == 'timeout') {
                getUserInfoAjax.abort();
                $.toast('系统繁忙');
                return;
            }

            if (resp.responseJSON.failed) {
                $.toast(resp.responseJSON.messages[0]);
            } else {
                $.toast('系统错误');
            }
        }
    });

} else {
    j('#log-in-out').text('登录');
    j('#log-in-out').unbind();
    j('#log-in-out').click(
        function () {
            window.localStorage.from = window.location.href;
        }
    );
    j('#log-in-out').attr('href', './htmls/sign_in.html');
    j('#register').show();
    j('#modify-pwd').hide();
    j('#edit-user-info').hide();
    j('#my-shops').hide();
    j('#my-message').hide();
    j('#hi').text('hi');
}

j('#register').click(
    function () {
        window.localStorage.from = window.location.href;
    }
);

j('#modify-pwd').click(
    function () {
        window.localStorage.from = window.location.href;
    }
);

j('#edit-user-info').click(function () {
    window.localStorage.from = window.location.href;
});

if (window.localStorage.from == 'pwd_update') {
    $.toast('修改成功');
    window.localStorage.from = null;
}

if (window.localStorage.from == 'register') {
    $.toast('注册成功');
    window.localStorage.from = null;
}

if (window.localStorage.from == 'user_update') {
    $.toast('修改成功');
    window.localStorage.from = null;
}
