var myName;
var ownerId = getOwnerIdFromHref();
var ownerName;
var myAvator;
var ownerAvator;

var hasNextPage = false;
var currentPage = 1;
var pageSize = 10;

if (!(window.localStorage.jwt == undefined || window.localStorage.jwt == null || window.localStorage.jwt == '')) {
    j('#log-in-out').text('退出登录');
    j('#log-in-out').click(
        function () {
            window.localStorage.removeItem('jwt');
        }
    );
    j('#log-in-out').attr('href', './index.html');
    j('#register').hide();
    j('#modify-pwd').show();
    j('#edit-user-info').show();
    j('#my-shops').show();
    j('#my-message').show();
    var getUserInfoAjax = j.ajax({
        url: backendAddr + "/user",
        type: 'get',
        contentType: 'application/json; charset=utf-8',
        timeout: 5000,
        async: false,
        headers: {
            Authorization: AUTHORIZATION_PREFIX + window.localStorage.jwt
        },
        success: function (data) {
            j('#hi').text('hi, ' + data.nickname);
            myName = data.nickname;
            myAvator = data.avatorFileName;
        },
        error: function (resp) {
            if (resp.statusText == 'timeout') {
                getUserInfoAjax.abort();
                $.toast('系统繁忙');
                return;
            }

            if (resp.responseJSON.failed) {
                $.toast(resp.responseJSON.messages[0]);
            } else {
                $.toast('系统错误');
            }
        }
    });

} else {
    j('#log-in-out').text('登录');
    j('#log-in-out').unbind();
    j('#log-in-out').click(
        function () {
            window.localStorage.from = window.location.href;
        }
    );
    j('#log-in-out').attr('href', '../htmls/sign_in.html');
    j('#register').show();
    j('#modify-pwd').hide();
    j('#edit-user-info').hide();
    j('#my-shops').hide();
    j('#my-message').hide();
    j('#hi').text('hi');
}

var getOwnerInfoAjax = j.ajax({
    url: backendAddr + "/user?id=" + ownerId,
    type: 'get',
    contentType: 'application/json; charset=utf-8',
    timeout: 5000,
    async: false,
    headers: {
        Authorization: AUTHORIZATION_PREFIX + window.localStorage.jwt
    },
    success: function (data) {
        ownerName = data.nickname;
        ownerAvator = data.avatorFileName;
        j('title').text(ownerName);
        j('h1.title').text(ownerName);
    },
    error: function (resp) {
        if (resp.statusText == 'timeout') {
            getOwnerInfoAjax.abort();
            $.toast('系统繁忙');
            return;
        }

        if (resp.responseJSON.failed) {
            $.toast(resp.responseJSON.messages[0]);
        } else {
            $.toast('系统错误');
        }
    }
});

var getHistoryAjax = j.ajax({
    url: backendAddr + "/message-history?to=" + ownerId + '&page=' + currentPage + '&size=' + pageSize,
    type: 'get',
    contentType: 'application/json; charset=utf-8',
    timeout: 5000,
    async: false,
    headers: {
        Authorization: AUTHORIZATION_PREFIX + window.localStorage.jwt
    },
    success: function (data) {
        if (data.list.length > 0) {
            for (var i = data.list.length - 1; i >= 0; i--) {
                var sender;
                var img = '<img src="../images/default_avator.png" style="width: 2.2rem; height: 2.2rem;">';
                if (ownerId == data.list[i].from) {
                    sender = ownerName;
                    if (ownerAvator != null) {
                        img = '<img src="' + backendAddr + '/image/user-profile/' + ownerAvator + '" style="width: 2.2rem; height: 2.2rem;">';
                    }
                } else {
                    sender = myName;
                    if (myAvator != null) {
                        img = '<img src="' + backendAddr + '/image/user-profile/' + myAvator + '" style="width: 2.2rem; height: 2.2rem;">';
                    }
                }
                var msg = j('<li><div class="item-content" style="align-items: normal"><div style="margin-top: 15px">' + img +
                    '</div><div style="width: 100%; margin-left: 15px">' +
                    '<div style="width: 100%;font-size: 16px">' + sender + '&nbsp&nbsp' + timeStampToTime(data.list[i].sendTime) + '</div>' +
                    '<div style="width: 100%">' + data.list[i].message.replace(/[\n\r]/g, '<br/>') + '<br/><br/></div></div></div></li>');
                j('#msg-list').append(msg);
            }
            j('.content').scrollTop(j('.content')[0].scrollHeight);
            currentPage++;
            hasNextPage = data.hasNextPage;
            if (data.hasNextPage) {
                j('#more').text('点击查看更多');
            } else {
                j('#more').text('没有更多了');
            }
        }
    },
    error: function (resp) {
        if (resp.statusText == 'timeout') {
            getHistoryAjax.abort();
            $.toast('系统繁忙');
            return;
        }

        if (resp.responseJSON.failed) {
            $.toast(resp.responseJSON.messages[0]);
        } else {
            $.toast('系统错误');
        }
    }
});

j('#more').click(function () {
    if (!hasNextPage) {
        return;
    }
    var getHistoryAjax = j.ajax({
        url: backendAddr + "/message-history?to=" + ownerId + '&page=' + currentPage + '&size=' + pageSize,
        type: 'get',
        contentType: 'application/json; charset=utf-8',
        timeout: 5000,
        headers: {
            Authorization: AUTHORIZATION_PREFIX + window.localStorage.jwt
        },
        success: function (data) {
            if (data.list.length > 0) {
                for (var i = 0; i < data.list.length; i++) {
                    var sender;
                    var img = '<img src="../images/default_avator.png" style="width: 2.2rem; height: 2.2rem;">';
                    if (ownerId == data.list[i].from) {
                        sender = ownerName;
                        if (ownerAvator != null) {
                            img = '<img src="' + backendAddr + '/image/user-profile/' + ownerAvator + '" style="width: 2.2rem; height: 2.2rem;">';
                        }
                    } else {
                        sender = myName;
                        if (myAvator != null) {
                            img = '<img src="' + backendAddr + '/image/user-profile/' + myAvator + '" style="width: 2.2rem; height: 2.2rem;">';
                        }
                    }
                    var msg = j('<li><div class="item-content" style="align-items: normal"><div style="margin-top: 15px">' + img +
                        '</div><div style="width: 100%; margin-left: 15px">' +
                        '<div style="width: 100%;font-size: 16px">' + sender + '&nbsp&nbsp' + timeStampToTime(data.list[i].sendTime) + '</div>' +
                        '<div style="width: 100%">' + data.list[i].message.replace(/[\n\r]/g, '<br/>') + '<br/><br/></div></div></div></li>');
                    j('#msg-list').prepend(msg);
                }
                currentPage++;
                hasNextPage = data.hasNextPage;
                if (data.hasNextPage) {
                    j('#more').text('点击查看更多');
                } else {
                    j('#more').text('没有更多了');
                }
            }
        },
        error: function (resp) {
            if (resp.statusText == 'timeout') {
                getHistoryAjax.abort();
                $.toast('系统繁忙');
                return;
            }

            if (resp.responseJSON.failed) {
                $.toast(resp.responseJSON.messages[0]);
            } else {
                $.toast('系统错误');
            }
        }
    });
});

var client = new WebSocket('ws://47.106.158.254:8080/chat');
client.onmessage = function (e) {
    if (e.data == 'ACK') {
        console.log(e.data);
        return;
    }

    var msgJson = j.parseJSON(e.data);
    var img = '<img src="../images/default_avator.png" style="width: 2.2rem; height: 2.2rem;">';
    if (ownerAvator != null) {
        img = '<img src="' + backendAddr + '/image/user-profile/' + ownerAvator + '" style="width: 2.2rem; height: 2.2rem;">';
    }
    var msg = j('<li><div class="item-content" style="align-items: normal"><div style="margin-top: 15px">' + img +
        '</div><div style="width: 100%; margin-left: 15px">' +
        '<div style="width: 100%; font-size: 16px">' + ownerName + '&nbsp&nbsp' + timeStampToTime(msgJson.sendTime) + '</div>' +
        '<div style="width: 100%">' + msgJson.message.replace(/[\n\r]/g, '<br/>') + '<br/><br/></div></div></div></li>');
    j('#msg-list').append(msg);
    msg.find('div')[0].scrollIntoView(false);
};
window.onbeforeunload = function (e) {
    client.close();
};
client.onopen = function (e) {
    client.send(window.localStorage.jwt);
};

j('#me').click(
    function () {
        $.openPanel('#right-panel');
    }
);

j('#register').click(
    function () {
        window.localStorage.from = window.location.href;
    }
);

j('#modify-pwd').click(
    function () {
        window.localStorage.from = window.location.href;
    }
);

j('#edit-user-info').click(function () {
    window.localStorage.from = window.location.href;
});

j('#my-message a').click(function () {
    window.localStorage.from = window.location.href;
    window.location.href = '../htmls/my_messages.html';
});

j('#footer-send').click(function () {
    if (trim(j('#footer-input').val()) == '') {
        j('#footer-input').val('');
        return;
    }

    client.send(JSON.stringify({
        to: ownerId,
        message: j('#footer-input').val()
    }));
    var img = '<img src="../images/default_avator.png" style="width: 2.2rem; height: 2.2rem;">';
    if (myAvator != null) {
        img = '<img src="' + backendAddr + '/image/user-profile/' + myAvator + '" style="width: 2.2rem; height: 2.2rem;">';
    }
    var msg = j('<li><div class="item-content" style="align-items: normal"><div style="margin-top: 15px">' + img +
        '</div><div style="width: 100%; margin-left: 15px">' +
        '<div style="width: 100%; font-size: 16px">' + myName + '&nbsp&nbsp' + timeStampToTime((new Date()).getTime()) + '</div>' +
        '<div style="width: 100%">' + j('#footer-input').val().replace(/[\n\r]/g, '<br>') + '<br/><br/></div></div></div></li>');
    j('#msg-list').append(msg);
    msg.find('div')[0].scrollIntoView(false);
    j('#footer-input').val('');
});

if (window.localStorage.from == 'pwd_update') {
    $.toast('修改成功');
    window.localStorage.from = null;
}

if (window.localStorage.from == 'register') {
    $.toast('注册成功');
    window.localStorage.from = null;
}

if (window.localStorage.from == 'user_update') {
    $.toast('修改成功');
    window.localStorage.from = null;
}
